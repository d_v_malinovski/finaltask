package com.epam.malinovski.controller.command.impl.user;


import com.epam.malinovski.bean.User;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Method to redirect lot update
 */
public class RedirectToUpdateUser implements Command {
    private static Logger logger = Logger.getLogger(RedirectToUpdateUser.class);
    private static final UserService userService = UserServiceImpl.getUserService();

    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)) {

            int id = Integer.parseInt(request.getParameter(Attributes.FIELD_USER_ID));
            User user = null;

            try {
               user = userService.getUserById(id);
            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
                return PagePathes.PATH_TO_ERROR_PAGE;
            }

            request.getSession().setAttribute(Attributes.USER, user);

            return PagePathes.PATH_TO_UPDATE_USER_PAGE;

        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }

}
