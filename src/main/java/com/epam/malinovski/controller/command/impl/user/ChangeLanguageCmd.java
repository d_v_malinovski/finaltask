package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.LangConstants;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.util.ContentManager;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;

/**
 * @author Denis
 * change locale of application
 */
public class ChangeLanguageCmd implements Command {
    private static Logger logger = Logger.getLogger(ChangeLanguageCmd.class);

    public String execute(HttpServletRequest request) {
        String lang = request.getParameter(LangConstants.LANG_BUTTON);
        String path = request.getParameter(LangConstants.PATH);

        if (lang.equals(LangConstants.ATTRIBUTE_BUTTON_RU)) {
            Config.set(request.getSession(),Config.FMT_LOCALE, LangConstants.RU);
            request.getSession().setAttribute(LangConstants.BUNDLE, LangConstants.RU);
            request.getSession().setAttribute(Attributes.CURRENT_LOCALE,LangConstants.RU);
        } else if (lang.equals(LangConstants.ATTRIBUTE_BUTTON_EN)) {
            Config.set(request.getSession(),Config.FMT_LOCALE, LangConstants.EN);
            request.getSession().setAttribute(LangConstants.BUNDLE, LangConstants.EN);
            request.getSession().setAttribute(Attributes.CURRENT_LOCALE,LangConstants.EN);
        } else {
            logger.error(Messages.UNCORRECT_OPERATION + path);
            Messager.sendMessage(request, Messages.LANG_ERROR);
        }

        ContentManager.addContent(path, request);

        return path;
    }
}
