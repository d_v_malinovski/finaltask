package com.epam.malinovski.controller.command.impl.user;


import com.epam.malinovski.bean.User;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.LangConstants;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This user create new user
 */
public class AddUserCmd implements Command {
    private static Logger logger = Logger.getLogger(AddUserCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();

    public String execute(HttpServletRequest request) {

    try {

            User user = new User();

            String login = request.getParameter(Attributes.LOGIN);
            String password = request.getParameter(Attributes.PASSWORD);
            String role = request.getParameter(Attributes.ROLE_ID);
            String language = request.getParameter(Attributes.LANGUAGE);

            user.setLogin(login);
            user.setPassword(password);

            if(role.equals(Attributes.ADMIN)){
                user.setRoleId(Attributes.ROLE_ADMIN);
            }
            if(role.equals(Attributes.USER)){
                user.setRoleId(Attributes.ROLE_USER);
            }
            if(language.equals(Attributes.RUSSIAN)){
                user.setLanguage(LangConstants.RU);
            }
            if(language.equals(Attributes.ENGLISH)){
                user.setLanguage(LangConstants.EN);
            }

            userService.save(user);

        } catch (ServiceException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            Messager.sendMessage(request, Messages.USER_IS_ALREADY_EXIST);
            return PagePathes.PATH_TO_ADD_USER_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_ADMIN_PAGE);

        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
