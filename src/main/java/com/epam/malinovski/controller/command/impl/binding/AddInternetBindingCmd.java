package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.constant.*;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author Denis
 * This method create internet auction
 */
public class AddInternetBindingCmd implements Command {
    private static Logger logger = Logger.getLogger(AddInternetBindingCmd.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)) {
         try {
            int lotId = Integer.parseInt(request.getParameter(Attributes.VARIANT_LOT_ID));

             String start = request.getParameter(Attributes.DATE_BEGIN) + " " + request.getParameter(Attributes.TIME_BEGIN);
             String end = request.getParameter(Attributes.DATE_END) + " " + request.getParameter(Attributes.TIME_END);


            Timestamp startDate = Timestamp.valueOf(start);
            Timestamp endDate = Timestamp.valueOf(end);
            Timestamp currentDate = new Timestamp(Calendar.getInstance().getTime().getTime());

            Binding binding = new Binding();
            binding.setStart(startDate);
            binding.setEnd(endDate);
            binding.setType(DBFields.BINDING_INTERNET);
            binding.setState(DBFields.BINDING_OPEN);

            if(startDate.after(endDate) || startDate.equals(endDate) || startDate.before(currentDate)){
                Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
                return PagePathes.PATH_TO_ADD_INTERNET_BINDING;
            }

             bindingService.save(lotId, binding);

         } catch (ServiceException | IllegalArgumentException e) {
                logger.error(e.getMessage(),e);
                Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
                return PagePathes.PATH_TO_ADD_INTERNET_BINDING;
         }
            request.getSession().removeAttribute(Attributes.LOT);

            request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_ADMIN_PAGE);
        }

        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
