/**
 * Created by Denis on 01.08.2017.
 */
$(function () {   
    
  $('#create_binding_form').validate({
      rules:{
          date_begin:{
              required: true,
              date: true
          },
          
          time_begin:{
              required: true
          },
          
          date_end: {
              required: true,
              date: true
          },
          
          time_end: {
              required: true
          },
          
          blitz_price: {
              required: true,
              min: 1
          },
          
          binding_step: {
              required: true,
              min: 1
          },
          
          number_of_participant: {
              required: true,
              min: 2
          },
          
          end_price:{
              required: true,
              min: 1
          }
          
      },
      errorClass: 'has-error',
      onsubmit: true

  });
    
});
