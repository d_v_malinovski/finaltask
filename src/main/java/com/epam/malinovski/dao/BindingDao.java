package com.epam.malinovski.dao;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.bean.Lot;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Denis
 */
public interface BindingDao extends GenericDao<Binding> {
    @Override
    default void insert(Binding data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default void update(Binding data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default void delete(Binding data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default List<Binding> select(Binding data) throws DaoException, SQLException {
        throw new UnsupportedOperationException();
    }

    void addInternetBinding(int lotId, Binding binding) throws DaoException;
    void addEnglishBinding(int lotId, Binding binding) throws DaoException;
    void addBlitzBinding(int lotId, Binding binding) throws DaoException;
    List<Lot> getActiveBindings(int userId) throws DaoException;
    List<Lot> getActiveBindings() throws DaoException;
    void deleteBindingByID(int id) throws DaoException;
    List<Lot> getBindingsWithOpenStateAndType(int statusId) throws DaoException;
    void closeBinding(int bindingId, int userId, int lotId) throws DaoException;
    Binding getBindingById(int id) throws DaoException;
}
