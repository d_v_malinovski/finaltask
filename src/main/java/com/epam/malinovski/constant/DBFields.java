package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class DBFields {

    //user
    public static final String ID = "id";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ROLE_ID = "role_id";
    public static final String ROLE_NAME_USER = "USER";
    public static final String ROLE_NAME_ADMIN = "ADMIN";
    public static final String PREFORM_LANG = "preform_language";
    //lot
    public static final String LOT_STATE = "lot_state_id";
    public static final String LOT_NAME = "name";
    public static final String LOT_PRICE = "current_price";
    public static final String LOT_DESCRIPTION = "description";
    public static final String OWNER = "user_id";
    public static final String LOT_ID = "lot_id";
    //type status for lots
    public static final int LOT_ABORTED_SALE = 3;
    public static final int LOT_STATE_FOR_SALE = 4;
    public static final int LOT_BLOCKING = 2;
    public static final int LOT_NOT_FOR_SALE = 5;
    public static final int LOT_SOLD = 1;

    public static final String LOT_ABORTED_SALE_STATE = "ABORTED SALE";
    public static final String LOT_STATE_FOR_SALE_STATE = "SEND TO SALE";
    public static final String LOT_BLOCKING_STATE = "BLOCKING";
    public static final String LOT_NOT_FOR_SALE_STATE = "NOT FOR SALE";
    public static final String LOT_SOLD_STATE = "SOLD";

    //binding state
    public static final int BINDING_OPEN = 1;
    public static final int BINDING_CLOSE = 2;

    public static final String BINDING_OPEN_STATE = "OPEN";
    public static final String BINDING_CLOSE_STATE = "CLOSED";

    //binding type
    public static final int BINDING_INTERNET = 3;
    public static final int BINDING_ENGLISH = 1;
    public static final int BINDING_BLITZ = 2;

    public static final String BINDING_INTERNET_STATE = "INTERNET-AUCTION";
    public static final String BINDING_ENGLISH_STATE = "ENGLISH-AUCTION";
    public static final String BINDING_BLITZ_STATE = "BLITZ-AUCTION";

    public static final String BINDING_STATE = "binding_state_id" ;
    public static final String BINDING_TYPE = "binding_type";
    public static final String BINDING_START_DATE = "start_date" ;
    public static final String BINDING_END_DATE = "end_date";
    public static final String BINDING_BLITZ_PRICE = "blitz_price";
    public static final String BINDING_STEP_PRICE="binding_step";
    public static final String BINDING_END_PRICE = "end_price";
    public static final String BINDING_NUMBER_OF_PARTICIPANT = "number_of_participant";
    public static final String BINDING_ID = "binding_id";
    // bet
    public static final String BET_FROM_USER = "user_id";
    public static final String BET_TO_BINDING = "binding_id";
    public static final String BET_CURRENT_TIME = "bet_time";
    public static final String BET_AMOUNT = "bet_amount";
    // basket
    public static final String BASKET_USER_WINNER_ID = "user_id";
    public static final String BASKET_LOT_ID = "lot_id";
    public static final String BASKET_OWNER_BILLING = "billing_id";
    public static final String MONEY_AMOUNT = "money_amount";


}
