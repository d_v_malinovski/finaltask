package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class DBConstant {
    public static final String CONNECTION_NUMBER = "db.poolsize";
    public static final String DRIVER = "db.driver";
    public static final String URL = "db.url";
    public static final String USER = "db.user";
    public static final String PASSWORD = "db.password";
}
