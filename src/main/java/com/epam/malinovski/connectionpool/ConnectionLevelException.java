package com.epam.malinovski.connectionpool;

/**
 * @author Denis
 */
public class ConnectionLevelException extends Exception {

    public ConnectionLevelException(String message){
        super(message);
    }

    public ConnectionLevelException(String message, Exception e){
        super(message, e);
    }
}
