package com.epam.malinovski.listener;


import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.connectionpool.Pool;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.util.BindingStateChecker;
import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import org.apache.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author Denis
 * This class manage context of appliaction init connection pool and job executor for close binding
 */
public class ManagementContextListener implements ServletContextListener {
    private static Logger logger = Logger.getLogger(ManagementContextListener.class);
    private Scheduler scheduler;

    private static String TYPE_TASK = "eventTask";
    private static String TRIGGER_NAME = "trigger1";
    private static String JOB_NAME = "my job";
    private static String INTERVAL_FOR_TASK = "10 0/2 * * * ?";

    private Pool connectionPool;

    public void contextInitialized(ServletContextEvent servletContextEvent) {

        try {
            connectionPool = ConnectionPool.getInstance();
            connectionPool.init();
        } catch (ConnectionLevelException e) {
            logger.error(e);
        }

        SchedulerFactory sf = new StdSchedulerFactory();
        try {
            scheduler = sf.getScheduler();
            CronTrigger trigger = newTrigger()
                    .withIdentity(TRIGGER_NAME, TYPE_TASK)
                    .withSchedule(cronSchedule(INTERVAL_FOR_TASK))
                    .build();

            JobDetail job =  newJob(BindingStateChecker.class)
                    .withIdentity(JOB_NAME, TYPE_TASK)
                    .build();

            scheduler.scheduleJob(job, trigger);
            scheduler.start();
        } catch (SchedulerException e) {
            logger.error(e.getMessage(),e);
        }
    }


    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            connectionPool.dispose();
        } catch (ConnectionLevelException e) {
            logger.error(e);
        }

        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            logger.error(e.getMessage(),e);
        }

    }
}
