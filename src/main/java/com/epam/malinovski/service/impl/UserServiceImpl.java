package com.epam.malinovski.service.impl;

import com.epam.malinovski.bean.User;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.DaoFactory;
import com.epam.malinovski.dao.UserDao;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;

import java.util.List;

/**
 * @author Denis
 */
public class UserServiceImpl implements UserService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    private UserServiceImpl(){

    }

    private static class SingletonHolder{
        private static final UserService userService = new UserServiceImpl();
    }

    public static UserService getUserService(){
        return SingletonHolder.userService;
    }

    @Override
    public void save(User data) throws ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();
            userDao.insert(data);
        }catch (DaoException ex){
            throw new ServiceException("User Service", ex);
        }
    }

    @Override
    public void update(User date) throws ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();
            userDao.update(date);
        }catch (DaoException ex){
            throw new ServiceException("User Service",ex);
        }
    }

    @Override
    public void delete(int id) throws ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();
            userDao.deleteUserById(id);
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }
    }

    @Override
    public List<User> getAll(int id) throws ServiceException {
        List<User> users;
        try {
            UserDao userDao = daoFactory.getUserDao();
            users = userDao.getAllUsers(id);
            return users;
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }
    }

    @Override
    public User getUserByLogin(String login) throws ServiceException {
        User user;
        try {
            UserDao userDao = daoFactory.getUserDao();
            user = userDao.getUserByLogin(login);
            return user;
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }
    }

    @Override
    public User getUserById(int id) throws ServiceException {
        User user;
        try {
            UserDao userDao = daoFactory.getUserDao();
            user = userDao.getUserById(id);
            return user;
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }

    }

    @Override
    public boolean checkMemberShip(int user_id, int binding_id) throws ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();
            return userDao.checkMemberShip(user_id,binding_id);
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }
    }

    @Override
    public void takePartInBinding(int user_id, int binding_id) throws ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();
            userDao.takePartInBinding(user_id,binding_id);
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }
    }

    @Override
    public boolean checkFreePositionForMemberShip(int binding_id) throws ServiceException {
        try {
            UserDao userDao = daoFactory.getUserDao();
            return userDao.checkFreePositionForMemberShip(binding_id);
        }catch (DaoException ex){
            throw new ServiceException("User Service Exception", ex);
        }
    }

}
