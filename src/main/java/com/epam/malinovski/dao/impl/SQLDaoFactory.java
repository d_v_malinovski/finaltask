package com.epam.malinovski.dao.impl;

import com.epam.malinovski.dao.*;

/**
 * Created by Denis on 05.08.2017.
 */
public class SQLDaoFactory extends DaoFactory {
    private static final SQLDaoFactory instance = new SQLDaoFactory();

    private SQLDaoFactory(){

    }

    public static SQLDaoFactory getInstance(){
        return instance;
    }

    @Override
    public UserDao getUserDao() {
        return SQLUserDao.getUserDao();
    }

    @Override
    public LotDao getLotDao() {
        return SQLLotDao.getLotDao();
    }

    @Override
    public BindingDao getBindingDao() {
        return SQLBindingDao.getBindingDao();
    }

    @Override
    public BillingDao getBillingDao() {
        return SQLBillingDao.getBillingDao();
    }

    @Override
    public BetDao getBetDao() {
        return SQLBetDao.getBetDao();
    }

    @Override
    public BasketDao getBasketDao() {
        return SQLBasketDao.getBasketDao();
    }
}
