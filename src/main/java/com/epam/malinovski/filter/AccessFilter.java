package com.epam.malinovski.filter;


import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.PagePathes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This class need to check access for current page
 */
public class AccessFilter {

    public static boolean isRoleTrue (HttpServletRequest request, String location){
        if((request.getSession() == null || request.getSession().getAttribute(AccessConstants.ROLE) == null) &&
                !location.equals(PagePathes.PATH_TO_LOGIN_PAGE)) {
            return false;
        }
        else if(request.getSession().getAttribute(AccessConstants.ROLE) == AccessConstants.ADMIN_ROLE
                && !(location.equals(PagePathes.PATH_TO_ADMIN_PAGE) ||
                     location.equals(PagePathes.PATH_TO_ADD_USER_PAGE) ||
                     location.equals(PagePathes.PATH_TO_UPDATE_USER_PAGE)||
                     location.equals(PagePathes.PATH_TO_ADD_INTERNET_BINDING)||
                     location.equals(PagePathes.PATH_TO_ADD_BLITZ_BINDING)||
                     location.equals(PagePathes.PATH_TO_ADD_ENGLISH_BINDING)||
                     location.equals(PagePathes.PATH_TO_BINDING_PAGE) ||
                     location.equals(PagePathes.PATH_TO_MANAGEMENT_USERS)
                     )) {
            return false;
        }
        else if(request.getSession().getAttribute(AccessConstants.ROLE) == AccessConstants.GUEST_ROLE
                && !(location.equals(PagePathes.PATH_TO_LOGIN_PAGE)||
                     location.equals(PagePathes.PATH_TO_REGISTRATION_PAGE))) {
            return false;
        }
        else if(request.getSession().getAttribute(AccessConstants.ROLE) == AccessConstants.USER_ROLE
                && !(location.equals(PagePathes.PATH_TO_USER_PAGE)||
                     location.equals(PagePathes.PATH_TO_ADD_LOT) ||
                     location.equals(PagePathes.PATH_TO_UPDATE_LOT)||
                     location.equals(PagePathes.PATH_TO_LOT_PAGE) ||
                     location.equals(PagePathes.PATH_TO_ENGLISH_BINDING_DETAIL) ||
                     location.equals(PagePathes.PATH_TO_INTERNET_BINDING_DETAIL) ||
                     location.equals(PagePathes.PATH_TO_BLITZ_BINDING_DETAIL) ||
                     location.equals(PagePathes.PATH_TO_BINDING_USER_MENU) ||
                     location.equals(PagePathes.PATH_TO_PAYMENT_LIST) ||
                     location.equals(PagePathes.PATH_TO_REPLENISH_BILLING)
                    )) {
            return false;
        }

        return true;
    }
}
