package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * @author Denis
 * This method update current lot
 */
public class UpdateLotCmd implements Command {
    private static Logger logger = Logger.getLogger(UpdateLotCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {

    try {
        int id = Integer.parseInt(request.getParameter(Attributes.LOT_ID));

            Lot lot = new Lot();

            lot.setLotId(id);

            String name = request.getParameter(Attributes.LOT_NAME);
            String description = request.getParameter(Attributes.LOT_DESCRIPTION);
            BigDecimal price = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.PRICE)));
            int status = Integer.parseInt(request.getParameter(Attributes.LOT_STATUS));

            lot.setName(name);
            lot.setPrice(price);
            lot.setDescription(description);
            lot.setState(status);

            lotService.update(lot);

        } catch (ServiceException| NumberFormatException | NullPointerException e) {
            logger.error(e.getMessage(),e);
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
            return PagePathes.PATH_TO_UPDATE_LOT;
        }

        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_LOT_PAGE);
        return PagePathes.PATH_TO_LOT_PAGE;
    }

}
