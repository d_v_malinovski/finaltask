<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 27.06.2017
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<html>
  <head>
    <fmt:setBundle basename="locale"/>
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="label.authorization"/> </title>

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru }">
          <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>

    <script src="${pageContext.request.contextPath}/js/loginForm.js"></script>
  </head>

  <body>
  <div class="container">

      <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
          <input type="hidden" name="cmd" value="changeLangCmd">
          <input type="hidden" name="path" value="login.jsp">

          <div class="form-group">
              <input class="form-control" type="submit" name="langBtn" value="ru">
          </div>
          <div class="form-group">
              <input class="form-control" type="submit" name="langBtn" value="en">
          </div>
      </form>

    <form id="login_form" class="form-signin" action="${pageContext.request.contextPath}/Control" method="post">
     <h2 class="form-signin-heading"><fmt:message key="label.authorization"/> </h2>

      <p id="message" class="has-error">${message}</p>

      <input minlength="4" maxlength="50" type="text" class="form-control"  placeholder="<fmt:message key="label.login"/> " id="login" name="login" required />
      <label for="login"></label>

      <input minlength="4" maxlength="50"  type="password" class="form-control" placeholder="<fmt:message key="label.password"/>" id="password" name="password" required/>
        <label for="password"></label>

      <input  class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="label.sing.in"/>"/>

      <input type="hidden" name="cmd" value="loginCmd">
    </form>

      <form class="form-signin" method="get" action="Control">
          <input class="btn btn-lg btn-success btn-block" type="submit" value="<fmt:message key="label.registration"/> ">
          <input type="hidden" name="cmd" value="redirectToRegistrationCmd">
      </form>

    </div>
  </body>
</html>
