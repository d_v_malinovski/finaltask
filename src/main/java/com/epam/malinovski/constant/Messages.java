package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class Messages {
   public static final String AUTHORIZATION_ERROR = "authorization.error";
   public static final String LANG_ERROR = "lang.error";
   public static final String UNCORRECT_INFORMATION = "uncorrect.information";
   public static final String UNCORRECT_OPERATION = "uncorrect.operation";

   public static final String USER_IS_ALREADY_EXIST ="user.exist";
}
