package com.epam.malinovski.controller.command.impl.payment;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BasketService;
import com.epam.malinovski.service.BillingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BasketServiceImpl;
import com.epam.malinovski.service.impl.BillingServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Method get page with payments
 */
public class RedirectToPaymentListCmd implements Command {
    private static Logger logger = Logger.getLogger(RedirectToPaymentListCmd.class);
    private static final BillingService billingService = BillingServiceImpl.getBillingService();
    private static final BasketService basketService = BasketServiceImpl.getBasketService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {

            int userId = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);


            try {
                request.getSession().setAttribute(Attributes.BILLING_STATE,
                        billingService.getBillingByUserId(userId).getMoneyAmount());
                request.setAttribute(Attributes.PAYMENTS, basketService.getAllProductToPayment(userId));

            } catch (ServiceException e) {
                e.printStackTrace();
            }
            return PagePathes.PATH_TO_PAYMENT_LIST;

        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
