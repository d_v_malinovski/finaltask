package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.bean.User;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * @author Denis
 * This method need to create new lot
 */
public class AddLotCmd implements Command {
    private static Logger logger = Logger.getLogger(AddLotCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {
    try {

            int id = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);
            User user = new User();
            user.setId(id);

            BigDecimal price = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.PRICE)));
            String name = request.getParameter(Attributes.LOT_NAME);
            String description = request.getParameter(Attributes.LOT_DESCRIPTION);
            int state = DBFields.LOT_NOT_FOR_SALE;

            Lot lot = new Lot();

            lot.setUser(user);
            lot.setPrice(price);
            lot.setDescription(description);
            lot.setState(state);
            lot.setName(name);

            lotService.save(lot);

        } catch (ServiceException | NullPointerException| NumberFormatException e) {
            logger.error(e.getMessage(),e);
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
            return PagePathes.PATH_TO_ADD_LOT;
        }

        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_LOT_PAGE);

        return PagePathes.PATH_TO_LOT_PAGE;
    }
}
