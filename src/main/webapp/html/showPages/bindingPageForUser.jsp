<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 20.07.2017
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session"/>
    <title><fmt:message key="label.bindings"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">

        </div>

        <div class="collapse navbar-collapse">
            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.payment"/>">
                </div>
                <input type="hidden" name="cmd" value="redirectToPaymentCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.bindings" />">
                </div>
                <input type="hidden" name="cmd" value="redirectToBindingMenuCmd">
            </form>


            <form class="navbar-form navbar-left"  method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.lots"/>">
                </div>
                <input type="hidden" name="cmd" value="redirectToLotManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.show.bindings"/>">
                </div>
                <input type="hidden" name="cmd" value="getBindingForUserCmd">
            </form>

            <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
                <input type="hidden" name="cmd" value="changeLangCmd">
                <input type="hidden" name="path" value="html/showPages/bindingPageForUser.jsp">

                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="ru">
                </div>
                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="en">
                </div>
            </form>

            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.log.out"/>"/>
                </div>
                <input type="hidden" name="cmd" value="logOutCmd">
            </form>
        </div>
    </div>
</nav>
<div class="container-fluid">
<table class="table">

<caption class="text-center">
    <label class="h2"><fmt:message key="label.bindings"/></label>
</caption>

<c:forEach var="lot" items="${bindingList}">

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="lot.name"/></th>
    <td><c:out value="${lot.name}"/></td>
    </tr>

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="lot.description"/></th>
    <c:set var="description" value="${lot.description}"/>
    <td><textarea readonly="true" class="form-control text-center">${description}</textarea></td>
    </tr>

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="lot.price"/> </th>
    <td><c:out value="${lot.price}   $"/></td>
    </tr>

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="binding.state"/></th>
    <td>
        <c:choose>
            <c:when test="${lot.binding.state eq 1}">
                <fmt:message key="label.open"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="label.close"/>
            </c:otherwise>
        </c:choose>
    </td>
    </tr>

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="binding.type"/></th>
    <td>
        <c:choose>
            <c:when test="${lot.binding.type eq 1}">
                <fmt:message key="english.binding"/>
            </c:when>
            <c:when test="${lot.binding.type eq 2}">
                <fmt:message key="blitz.binding"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="internet.binding"/>
            </c:otherwise>
        </c:choose>
    </td>
    </tr>

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="binding.start"/></th>
    <c:set var="start" value="${lot.binding.start}"/>
    <td><fmt:formatDate type = "both" dateStyle = "long" timeStyle = "long"  value = "${start}"/></td>
    </tr>

    <tr class="text-center">
    <th class="col-xs-3"><fmt:message key="binding.end"/></th>
    <c:set var="end" value="${lot.binding.end}"/>
    <td><fmt:formatDate type = "both" dateStyle = "long" timeStyle = "long"  value = "${end}"/></td>
    </tr>
        <tr class="text-left">
            <td>
              <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input class="btn btn-success" type="submit" value="<fmt:message key="label.add.bet"/> ">
                    <input type="hidden" name="cmd" value="redirectToBindDetailCmd">
                    <input type="hidden" name="lot_id" value="<c:out value="${lot.lotId}"/>">
                    <input type="hidden" name="bindingId" value="<c:out value="${lot.binding.id}"/>">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
</div>
</body>
</html>
