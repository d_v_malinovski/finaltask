package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * redirect to auction detail
 */
public class RedirectToBindDetailCmd implements Command {
    private Logger logger = Logger.getLogger(RedirectToBindDetailCmd.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {
            if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {

                int lot_id = Integer.parseInt(request.getParameter(Attributes.VARIANT_LOT_ID));
                int binding_id = Integer.parseInt(request.getParameter(Attributes.BINDING_ID));

                Lot lot = null;
                Binding binding = null;
                try {
                    lot = lotService.getLotById(lot_id);
                    binding = bindingService.getBindingById(binding_id);
                    lot.setBinding(binding);

                } catch (ServiceException e) {
                    logger.error(e.getMessage(),e);
                }

                request.getSession().setAttribute(Attributes.LOT, lot);

                if(binding.getType() == DBFields.BINDING_ENGLISH)
                    return PagePathes.PATH_TO_ENGLISH_BINDING_DETAIL;
                if(binding.getType() == DBFields.BINDING_BLITZ)
                    return PagePathes.PATH_TO_BLITZ_BINDING_DETAIL;
                if(binding.getType() == DBFields.BINDING_INTERNET)
                    return PagePathes.PATH_TO_INTERNET_BINDING_DETAIL;

            }
                return PagePathes.PATH_TO_ERROR_PAGE;
    }

}
