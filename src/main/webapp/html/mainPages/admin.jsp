<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 03.07.2017
  Time: 14:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib uri="/WEB-INF/mytag.tld" prefix="m" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <fmt:setBundle basename="locale"/>
    <title><fmt:message key="label.admin.page"/> </title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-form navbar-left">
            <div class="form-group">
                <input class="form-control" readonly="true" value="<m:today/>">
            </div>
        </div>

        <div class="collapse navbar-collapse">

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.users"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToUsersManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.bindings"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToBindingManagementCmd">
            </form>


            <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
                <input type="hidden" name="cmd" value="changeLangCmd">
                <input type="hidden" name="path" value="html/mainPages/admin.jsp">

                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="ru">
                </div>
                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="en">
                </div>
            </form>

            <%-- Выйти из приложения --%>
            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.log.out"/>"/>
                </div>
                <input type="hidden" name="cmd" value="logOutCmd">
            </form>
        </div>
    </div>
</nav>
</body>
</html>
