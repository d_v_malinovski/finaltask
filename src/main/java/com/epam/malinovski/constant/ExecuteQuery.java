package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class ExecuteQuery {

    public static final String SELECT_USER_BY_LOGIN = "SELECT id, login, password, role_id, preform_language" +
                                                        " FROM user WHERE login = ?";
    public static final String GET_ALL_USERS = "SELECT id, login, password, role_id FROM user WHERE user.id != ?";
    public static final String DELETE_USER_BY_ID = "DELETE FROM final_task.user WHERE id = ?";
    public static final String SELECT_USER_BY_ID = "SELECT id, login, password, role_id FROM user WHERE id = ?";
    public static final String UPDATE_USER_BY_ID = "UPDATE final_task.user SET login = ?, password = ?," +
                                                    "role_id = ? WHERE id = ?;";

    public static final String ADD_USER = "INSERT INTO `final_task`.`user` (`login`, `password`, " +
                                           "`role_id`,`preform_language`) VALUES (?, ?, ?, ?)";

    public static final String GET_LAST_BILLING_ID = "SELECT count(id) FROM final_task.billing";

    public static final String GET_BILLING_BY_USER_ID = "SELECT billing.id, billing.money_amount, billing.user_id" +
                                                            " FROM billing WHERE billing.user_id = ?";

    public static final String SEND_CURRENT_SUM_TO_BILLING_BY_ID = "UPDATE final_task.billing SET  " +
                                            "billing.money_amount = (billing.money_amount + ?) WHERE billing.id = ?";


    public static final String ADD_BILLING = "INSERT INTO billing (user_id, money_amount) VALUES(?,?)";

    public static final String GET_LAST_LOT_ID = "SELECT id FROM lot ORDER BY id DESC LIMIT 1";
    public static final String GET_LOT_BY_ID = "SELECT lot.id, lot.lot_state_id, lot.name, lot.user_id," +
                                                " lot.current_price," +
                                                "lot.description, lot.binding_id FROM lot WHERE lot.id = ?";

    public static final String SELECT_USERS_LOTS = "SELECT lot.id, lot.lot_state_id, lot.name, lot.user_id," +
                                                    " lot.current_price, " +
                                                    "lot.description " +
                                                    "FROM lot WHERE user_id = ? and lot_state_id != 1";

    public static final String UPDATE_LOT = "UPDATE final_task.lot SET current_price = ?, name = ?," +
                                             "description = ?, lot_state_id = ? WHERE id = ?";

    public static final String UPDATE_LOT_AFTER_BINDING = "UPDATE lot SET lot_state_id = ?, binding_id = ? WHERE lot.id = ?";

    public static final String CHANGE_LOT_STATE = "UPDATE final_task.lot SET lot.lot_state_id = ? where lot.id = ?";

    public static final String ADD_LOT = "INSERT INTO `final_task`.`lot` (`lot_state_id`, `user_id`, `name`," +
                                        "`current_price`, `description`) VALUES (?, ?, ?, ?, ?)";

    public static final String GET_ALL_LOT_FOR_SALE = "SELECT lot.id, lot.lot_state_id, lot.user_id, lot.name, " +
            "lot.current_price," +
            " lot.description, `user`.login FROM `final_task`.`lot` INNER JOIN `final_task`.`user`" +
            " ON lot.user_id = `user`.id and lot.lot_state_id = ?";


    public static final String GET_LAST_BINDING_ID = "SELECT id FROM binding ORDER BY id DESC LIMIT 1";

    public static final String ADD_INTERNET_BINDING = "INSERT INTO `final_task`.`binding`" +
                                             " (`id`, `binding_state_id`, `binding_type`, `start_date`, `end_date`)" +
                                             " VALUES (?, ?, ?, ?, ?)";

    public static final String ADD_ENGLISH_BINDING = "INSERT INTO `final_task`.`binding`" +
            " (`id`, `binding_state_id`, `binding_type`, `start_date`, `end_date`, `end_price`)" +
            " VALUES (?, ?, ?, ?, ?, ?)";

    public static final String ADD_BLITZ_BINDING = "INSERT INTO `final_task`.`binding`" +
            " (`id`, `binding_state_id`, `binding_type`, `start_date`, `end_date`," +
            " `binding_step`, `blitz_price`, `number_of_participant`)" +
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    public static final String GET_ACTIVE_BINDING = "SELECT binding.id, binding.binding_state_id, binding.binding_type," +
            " binding.start_date, binding.end_date" +
            ", lot.id as lot_id, lot.name, lot.description, lot.current_price, lot.user_id " +
            "FROM binding INNER JOIN lot WHERE binding.id = lot.binding_id AND" +
            " current_timestamp() < binding.end_date AND binding.binding_state_id = 1 AND lot.user_id != ? ";

    public static final String GET_ALL_ACTIVE_BINDING = "SELECT binding.id, binding.binding_state_id, binding.binding_type," +
            " binding.start_date, binding.end_date " +
            ", lot.id as lot_id, lot.name, lot.description, lot.current_price, lot.user_id " +
            "FROM binding INNER JOIN lot WHERE binding.id = lot.binding_id and " +
            "current_timestamp() < binding.end_date and binding.binding_state_id = 1 ";

    public static final String DELETE_BINDING_BY_ID = "DELETE FROM binding WHERE binding.id = ?";

    public static final String GET_BINDING_BY_ID = "SELECT binding.id, binding.binding_state_id, binding.binding_type," +
                        " binding.start_date, binding.end_date, binding.number_of_participant, binding_step, blitz_price," +
                        " end_price FROM binding WHERE binding.id = ?";

    public static final String ADD_BET = "INSERT INTO `final_task`.`bet` " +
                                            "(`user_id`, `binding_id`, `bet_time`, `bet_amount`) " +
                                            "VALUES (?, ?, ?, ?)";

    public static final String CHECK_USER_MEMBERSHIP_IN_BINDING = "SELECT user_id, binding_id FROM binding_user" +
                                                                    " WHERE user_id = ? and binding_id = ?";

    public static final String TAKE_PART_IN_BINDING = "INSERT INTO `final_task`.`binding_user` (`user_id`, `binding_id`)" +
                                                                                                " VALUES (?, ?)";

    public static final String UPDATE_PRICE_OF_LOT = "UPDATE final_task.lot SET current_price = ? WHERE id = ?";

    public static final String GET_BINDINGS_WITH_CURRENT_STATUS = "SELECT binding.id, binding.binding_state_id," +
                                                    " binding.binding_type, binding.start_date, binding.end_date, " +
                                    "lot.id as lot_id, lot.name, lot.description, lot.current_price, lot.user_id " +
                       "FROM binding INNER JOIN lot WHERE binding.id = lot.binding_id AND binding.binding_state_id = ? " +
                        "AND current_timestamp() >= binding.end_date ";

    public static final String CLOSE_BINDING = "UPDATE final_task.binding SET binding_state_id = ? WHERE id = ?";

    public static final String GET_LAST_BET = "SELECT bet.id, bet.user_id, bet.binding_id, bet.bet_time," +
                                                " bet.bet_amount, binding.end_date " +
                                                "FROM bet INNER JOIN binding WHERE binding.id = bet.binding_id " +
                                                "AND binding.end_date >= bet.bet_time " +
                                                "AND binding_id = ? ORDER BY bet_time DESC LIMIT 1";

    public static final String GET_ALL_PAYMENTS = "SELECT basket.id, basket.user_id, basket.lot_id, basket.billing_id," +
                                                    " lot.id, lot.name," +
                                                    " lot.current_price FROM basket INNER JOIN lot WHERE " +
                                                    "basket.lot_id = lot.id AND basket.user_id = ?";

    public static final String COMPUTE_COUNT_OF_PARTICIPANT = "SELECT count(user_id) AS number_of_participant " +
                                                            "FROM final_task.binding_user WHERE binding_id = ?";

    public static final String PUT_PRODUCT_FOR_PAYMENT = "INSERT INTO `final_task`.`basket` (`user_id`, `lot_id`, `billing_id`)" +
                                                                                            " VALUES (?, ?, ?)";

    public static final String GET_PRODUCT_OWNER_ID_BY_LOT_ID = "SELECT user_id FROM lot WHERE lot.id = ?";

    public static final String ADD_MONEY_TO_BILLING = "UPDATE billing SET money_amount = money_amount + ?" +
                                                                                            " WHERE billing.id = ?";
    public static final String SUBTRACT_MONEY_TO_BILLING = "UPDATE billing SET money_amount = money_amount - ? " +
                                                                                                "WHERE billing.id = ?";
    public static final String UPDATE_LOT_OWNER = "UPDATE lot set user_id = ? WHERE lot.id = ?";
    public static final String DELETE_PAYMENT = "DELETE FROM `final_task`.`basket` WHERE `id`= ?";
}
