package com.epam.malinovski.dao;

import com.epam.malinovski.bean.User;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Denis on 05.08.2017.
 */
public interface UserDao extends GenericDao<User> {
    @Override
    default List<User> select(User data) throws DaoException, SQLException {
        throw new UnsupportedOperationException();
    }
    @Override
    default void delete(User data) throws DaoException {
        throw new UnsupportedOperationException();

    }
    void deleteUserById(int id) throws DaoException;
    List<User> getAllUsers(int id) throws DaoException;
    User getUserByLogin(String login) throws DaoException;
    User getUserById(int id) throws DaoException;
    boolean checkMemberShip(int user_id, int binding_id) throws DaoException;
    void takePartInBinding(int user_id, int binding_id) throws DaoException;
    boolean checkFreePositionForMemberShip(int binding_id)throws DaoException;
}
