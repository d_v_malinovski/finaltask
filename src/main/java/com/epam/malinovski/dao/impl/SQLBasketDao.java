package com.epam.malinovski.dao.impl;

import com.epam.malinovski.bean.Basket;
import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ExecuteQuery;
import com.epam.malinovski.dao.BasketDao;
import com.epam.malinovski.dao.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * implements DAO interface for <code>Basket</code> entity where save information about payment
 * @author Denis
 */
public class SQLBasketDao implements BasketDao {
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private SQLBasketDao(){
    }

    public static BasketDao getBasketDao(){
        return SingletonHolder.basketDao ;
    }

    private static class SingletonHolder{
        private static final BasketDao basketDao = new SQLBasketDao();
    }
    /**
     * Delete information about payment
     * @param basketId - serial number of payment
     * @throws DaoException
     */
    public void deletePaymentById(int basketId) throws DaoException{

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.DELETE_PAYMENT)) {

            preparedStatement.setInt(1,basketId);
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
    /**
     * Get list with information about products witch must be pay
     * @param userId - current user
     * @return - payment list
     * @throws DaoException
     */

    public List<Basket> getAllProductToPayment(int userId) throws DaoException{
        List<Basket> products = new ArrayList<>();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_ALL_PAYMENTS)) {

            preparedStatement.setInt(1,userId);
            ResultSet resultSet = preparedStatement.executeQuery();


            while (resultSet.next()){
                Basket basket = new Basket();
                basket.setBasketId(resultSet.getInt(DBFields.ID));
                basket.setBillingId(resultSet.getInt(DBFields.BASKET_OWNER_BILLING));
                basket.setOwnerId(resultSet.getInt(DBFields.BASKET_USER_WINNER_ID));

                Lot lot = new Lot();

                lot.setLotId(resultSet.getInt(DBFields.BASKET_LOT_ID));
                lot.setName(resultSet.getString(DBFields.LOT_NAME));
                lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));

                basket.setProduct(lot);
                products.add(basket);
            }

        } catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return products;
    }
}
