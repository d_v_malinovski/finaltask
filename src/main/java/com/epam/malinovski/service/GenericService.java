package com.epam.malinovski.service;

import java.util.List;

/**
 * Created by Denis on 05.08.2017.
 */
public interface GenericService<T> {
    void save(T data) throws ServiceException;
    List<T> showAll() throws ServiceException;
}
