package com.epam.malinovski.service.impl;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.DaoFactory;
import com.epam.malinovski.dao.LotDao;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Denis
 */

public class LotServiceImpl implements LotService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    private LotServiceImpl(){

    }

    private static class SingletonHolder{
        private static final LotService lotService = new LotServiceImpl();
    }

    public static LotService getLotService(){
        return SingletonHolder.lotService;
    }



    @Override
    public void save(Lot data) throws ServiceException {
        try {
            LotDao lotDao = daoFactory.getLotDao();
              lotDao.insert(data);
        }catch (DaoException ex){
            throw new ServiceException("Lot Service",ex);
        }
    }

    @Override
    public void updateLotOwner(int lotId, int userId) throws ServiceException {
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lotDao.updateLotOwner(lotId, userId);
        }catch (DaoException ex){
            throw new ServiceException("Lot Service", ex);
        }
    }

    @Override
    public List<Lot> getAllLotByUserId(int userId) throws ServiceException {
        List<Lot> lots;
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lots = lotDao.getAllLotByUserId(userId);

            return lots;
        }catch (DaoException ex){
            throw new ServiceException("Lot Service" ,ex);
        }
    }

    @Override
    public Lot getLotById(int lotId) throws ServiceException {
        Lot lot;
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lot = lotDao.getLotById(lotId);
            return lot;
        }catch (DaoException ex){
            throw new ServiceException("Lot Service", ex);
        }
    }

    @Override
    public void changeLotState(int lotId, int stateId) throws ServiceException {
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lotDao.changeLotState(lotId, stateId);
        }catch (DaoException ex){
            throw new ServiceException("Lot Service", ex);
        }
    }

    @Override
    public List<Lot> getLotForSale() throws ServiceException {
        List<Lot> lots;
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lots = lotDao.getLotForSale();
            return lots;
        }catch (DaoException ex){
            throw new ServiceException("Lot Service", ex);
        }
    }

    @Override
    public void updatePriceLotById(int lotId, BigDecimal price) throws ServiceException {
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lotDao.updatePriceLotById(lotId, price);
        }catch (DaoException ex){
            throw  new ServiceException("Lot Service", ex);
        }
    }

    @Override
    public void update(Lot data) throws ServiceException {
        try {
            LotDao lotDao = daoFactory.getLotDao();
            lotDao.update(data);
        }catch (DaoException ex){
            throw new ServiceException("Lot Service", ex);
        }
    }
}
