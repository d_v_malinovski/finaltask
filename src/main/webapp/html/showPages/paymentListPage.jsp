<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 24.07.2017
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session"/>
    <title><fmt:message key="label.payment"/> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">

        </div>

        <div class="collapse navbar-collapse">
            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.payment"/>">
                </div>
                <input type="hidden" name="cmd" value="redirectToPaymentCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.bindings" />">
                </div>
                <input type="hidden" name="cmd" value="redirectToBindingMenuCmd">
            </form>

            <form class="navbar-form navbar-left"  method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.lots"/>">
                </div>
                <input type="hidden" name="cmd" value="redirectToLotManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.show.payments"/>">
                </div>
                <input type="hidden" name="cmd" value="getPaymentListCmd">
            </form>

            <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
                <input type="hidden" name="cmd" value="changeLangCmd">
                <input type="hidden" name="path" value="html/showPages/paymentListPage.jsp">

                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="ru">
                </div>
                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="en">
                </div>
            </form>

            <div class="navbar-form navbar-right">
                <div class="form-group">
                    <input class="form-control" readonly="true" value="<c:out value="${billingState} $"/>">
                </div>
            </div>

            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.add.money"/>"/>
                </div>
                <input type="hidden" name="cmd" value="redirectReplenishCmd">
            </form>

        <%-- Выйти из приложения --%>
            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.log.out"/>"/>
                </div>
                <input type="hidden" name="cmd" value="logOutCmd">
            </form>
        </div>
    </div>
</nav>



<table class="table">
    <tr class="text-center">
        <th class="text-center" ><fmt:message key="lot.name"/> </th>
        <th class="text-center"><fmt:message key="lot.price"/> </th>
        <th class="text-center"><fmt:message key="payment.operation"/> </th>
    </tr>
    <c:forEach var="basket" items="${paymentList}">
        <tr rowspan="2" class="text-right">
            <td class="text-center"><c:out value="${basket.product.name}"/></td>
            <td class="text-center"><c:out value="${basket.product.price} $"/></td>

            <td class="text-center">
                <form class="form-inline" method="post" action="${pageContext.request.contextPath}/Control">
                <input class="form-group btn btn-success" type="submit" value="<fmt:message key="set.payment"/>">
                <input type="hidden" name="basketId" value="${basket.basketId}">
                <input type="hidden" name="lotId" value="${basket.product.lotId}">
                <input type="hidden" name="newOwnerId" value="${basket.ownerId}">
                <input type="hidden" name="ownerBillingId" value="${basket.billingId}">
                <input type="hidden" name="cmd" value="payLotCmd">
            </form>
            </td>

            <td class="text-center">
                <form class="form-inline" method="post" action="${pageContext.request.contextPath}/Control">
                    <input class="btn btn-danger" type="submit" value="<fmt:message key="cancel.label"/> ">
                    <input type="hidden" name="basketId" value="${basket.basketId}">
                    <input type="hidden" name="lotId" value="${basket.product.lotId}">
                    <input type="hidden" name="cmd" value="cancelPaymentCmd">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<div class="container">
    <p class="has-error text-center" id="message">${message}</p>
</div>
</body>
</html>
