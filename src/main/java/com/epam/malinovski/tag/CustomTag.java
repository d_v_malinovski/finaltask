package com.epam.malinovski.tag;

import com.epam.malinovski.constant.LangConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author Denis
 * realise customized tag for show current time
 */
public class CustomTag extends TagSupport {
    private static Logger logger = Logger.getLogger(CustomTag.class);
    private Date current;
    private static String RUSSIAN_FORMAT = "dd:MM:yyyy k:mm:ss";
    private static String ENGLISH_FORMAT = "yyyy:dd:MM k:mm:ss" ;
    private static String COUNTRY_EN = "US";
    private static String EN_LANG = "en";

    @Override
    public int doStartTag() throws JspException {

        current = Calendar.getInstance().getTime();

        SimpleDateFormat russian = new SimpleDateFormat(RUSSIAN_FORMAT, Locale.getDefault());
        SimpleDateFormat english = new SimpleDateFormat(ENGLISH_FORMAT, new Locale(EN_LANG, COUNTRY_EN));

        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        HttpSession session = request.getSession();


        JspWriter out = pageContext.getOut();

        try{
            if(session.getAttribute(LangConstants.BUNDLE).equals(LangConstants.EN)) {
                out.print(english.format(current));
            }
            else {
                out.print(russian.format(current));
            }

        }catch(Exception e){
            logger.error(e.getMessage(),e);
        }
        return SKIP_BODY;
    }
}
