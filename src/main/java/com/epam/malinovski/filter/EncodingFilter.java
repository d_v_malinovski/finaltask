package com.epam.malinovski.filter;

import javax.servlet.*;
import java.io.IOException;
/**
 * @author Denis
 * Filter to set encoding param
 */
public class EncodingFilter implements Filter {
    private String encoding = "utf-8";
    private final String encodingParamName = "encoding";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        request.setCharacterEncoding(encoding);
        response.setCharacterEncoding(encoding);
        filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String encodingParam = filterConfig.getInitParameter(encodingParamName);
        if(encodingParam != null) {
            encoding = encodingParam;
        }
    }

    @Override
    public void destroy() {
    }
}
