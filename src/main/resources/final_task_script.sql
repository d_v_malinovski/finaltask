-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema final_task
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `final_task` ;

-- -----------------------------------------------------
-- Schema final_task
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `final_task` DEFAULT CHARACTER SET utf8 ;
USE `final_task` ;

-- -----------------------------------------------------
-- Table `final_task`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`user_role` ;

CREATE TABLE IF NOT EXISTS `final_task`.`user_role` (
  `id` INT(11) NOT NULL,
  `role_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `role_name` (`role_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`user_role` (`id`, `role_name`) VALUES ('1', 'user');
INSERT INTO `final_task`.`user_role` (`id`, `role_name`) VALUES ('2', 'admin');


-- -----------------------------------------------------
-- Table `final_task`.`user` 

-- login denis password denis 
-- login tania password tania
-- login sanny password sanny
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`user` ;

CREATE TABLE IF NOT EXISTS `final_task`.`user` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role_id` INT(10) NOT NULL,
  `preform_language` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login` (`login` ASC),
  INDEX `FK_user_role` (`role_id` ASC),
  CONSTRAINT `FK_user_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `final_task`.`user_role` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`user` (`id`, `login`, `password`, `role_id`, `preform_language`) VALUES ('1', 'denis', '37871d1305db2a323c4fc5f8afe23eccdda99', '1', 'ru_RU');
INSERT INTO `final_task`.`user` (`id`, `login`, `password`, `role_id`, `preform_language`) VALUES ('2', 'sanny', '40a17efd6f821e53b3b9c973d2e0d63b456fa', '1', 'en_US');
INSERT INTO `final_task`.`user` (`id`, `login`, `password`, `role_id`, `preform_language`) VALUES ('3', 'tania', '4b10d31c2c8f05b0b0cac1848d8bb6ae1ff42', '2', 'ru_RU');


-- -----------------------------------------------------
-- Table `final_task`.`billing`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`billing` ;

CREATE TABLE IF NOT EXISTS `final_task`.`billing` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `money_amount` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_billing` (`user_id` ASC),
  CONSTRAINT `fk_user_billing`
    FOREIGN KEY (`user_id`)
    REFERENCES `final_task`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`billing` (`id`, `user_id`, `money_amount`) VALUES ('1', '1', '100');
INSERT INTO `final_task`.`billing` (`id`, `user_id`, `money_amount`) VALUES ('2', '2', '100');


-- -----------------------------------------------------
-- Table `final_task`.`binding_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`binding_state` ;

CREATE TABLE IF NOT EXISTS `final_task`.`binding_state` (
  `id` INT(11) NOT NULL,
  `state_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `state_name` (`state_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`binding_state` (`id`, `state_name`) VALUES ('1', 'opened');
INSERT INTO `final_task`.`binding_state` (`id`, `state_name`) VALUES ('2', 'closed');


-- -----------------------------------------------------
-- Table `final_task`.`binding_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`binding_type` ;

CREATE TABLE IF NOT EXISTS `final_task`.`binding_type` (
  `id` INT(11) NOT NULL,
  `binding_type` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`binding_type` (`id`, `binding_type`) VALUES ('1', 'english');
INSERT INTO `final_task`.`binding_type` (`id`, `binding_type`) VALUES ('2', 'blitz');
INSERT INTO `final_task`.`binding_type` (`id`, `binding_type`) VALUES ('3', 'internet');


-- -----------------------------------------------------
-- Table `final_task`.`binding`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`binding` ;

CREATE TABLE IF NOT EXISTS `final_task`.`binding` (
  `id` INT(11) NOT NULL,
  `binding_state_id` INT(11) NOT NULL,
  `binding_type` INT(11) NOT NULL,
  `number_of_participant` INT(11) NULL DEFAULT NULL,
  `binding_step` DECIMAL(10,2) NULL DEFAULT NULL,
  `blitz_price` DECIMAL(10,2) NULL DEFAULT NULL,
  `end_price` DECIMAL(10,2) NULL DEFAULT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_binding_state` (`binding_state_id` ASC),
  INDEX `FK_binding_type` (`binding_type` ASC),
  CONSTRAINT `FK_binding_state`
    FOREIGN KEY (`binding_state_id`)
    REFERENCES `final_task`.`binding_state` (`id`),
  CONSTRAINT `FK_binding_type`
    FOREIGN KEY (`binding_type`)
    REFERENCES `final_task`.`binding_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`binding` (`id`, `binding_state_id`, `binding_type`, `start_date`, `end_date`) VALUES ('1', '1', '1', '2017-08-08 00:00:00', '2100-08-08 00:00:00');


-- -----------------------------------------------------
-- Table `final_task`.`lot_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`lot_state` ;

CREATE TABLE IF NOT EXISTS `final_task`.`lot_state` (
  `id` INT(11) NOT NULL,
  `name_state` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`lot_state` (`id`, `name_state`) VALUES ('1', 'sold');
INSERT INTO `final_task`.`lot_state` (`id`, `name_state`) VALUES ('2', 'blocked');
INSERT INTO `final_task`.`lot_state` (`id`, `name_state`) VALUES ('3', 'filmed');
INSERT INTO `final_task`.`lot_state` (`id`, `name_state`) VALUES ('4', 'for_sale');
INSERT INTO `final_task`.`lot_state` (`id`, `name_state`) VALUES ('5', 'non-for-sale');


-- -----------------------------------------------------
-- Table `final_task`.`lot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`lot` ;

CREATE TABLE IF NOT EXISTS `final_task`.`lot` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lot_state_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `binding_id` INT(11) NULL DEFAULT NULL,
  `name` VARCHAR(50) NOT NULL,
  `current_price` DECIMAL(10,2) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_lot_user` (`user_id` ASC),
  INDEX `FK_lot_state_lot` (`lot_state_id` ASC),
  INDEX `FK_lot_binding` (`binding_id` ASC),
  CONSTRAINT `FK_lot_binding`
    FOREIGN KEY (`binding_id`)
    REFERENCES `final_task`.`binding` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
  CONSTRAINT `FK_lot_state_lot`
    FOREIGN KEY (`lot_state_id`)
    REFERENCES `final_task`.`lot_state` (`id`),
  CONSTRAINT `FK_lot_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `final_task`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8; 

INSERT INTO `final_task`.`lot` (`id`, `lot_state_id`, `user_id`, `name`, `current_price`, `description`) VALUES ('1', '5', '1', 'Велосипед', '50', 'Очень хороший велосипед');
INSERT INTO `final_task`.`lot` (`id`, `lot_state_id`, `user_id`, `name`, `current_price`, `description`) VALUES ('2', '5', '1', 'Машина Игрушечная', '10', 'Состояние Б/У');
INSERT INTO `final_task`.`lot` (`id`, `lot_state_id`, `user_id`, `name`, `current_price`, `description`) VALUES ('3', '5', '2', 'Моющий пылесос', '30', 'Состояние Б/У');


-- -----------------------------------------------------
-- Table `final_task`.`basket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`basket` ;

CREATE TABLE IF NOT EXISTS `final_task`.`basket` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `lot_id` INT(11) NOT NULL,
  `billing_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_busket_user` (`user_id` ASC),
  INDEX `FK_busket_lot` (`lot_id` ASC),
  INDEX `FK_busket_billing` (`billing_id` ASC),
  CONSTRAINT `FK_busket_billing`
    FOREIGN KEY (`billing_id`)
    REFERENCES `final_task`.`billing` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_busket_lot`
    FOREIGN KEY (`lot_id`)
    REFERENCES `final_task`.`lot` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_busket_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `final_task`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `final_task`.`bet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`bet` ;

CREATE TABLE IF NOT EXISTS `final_task`.`bet` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `binding_id` INT(11) NOT NULL,
  `bet_time` DATETIME NOT NULL,
  `bet_amount` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_bet_user` (`user_id` ASC),
  INDEX `FK_bet_binding` (`binding_id` ASC),
  CONSTRAINT `FK_bet_binding`
    FOREIGN KEY (`binding_id`)
    REFERENCES `final_task`.`binding` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_bet_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `final_task`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `final_task`.`binding_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final_task`.`binding_user` ;

CREATE TABLE IF NOT EXISTS `final_task`.`binding_user` (
  `user_id` INT(11) NOT NULL,
  `binding_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `binding_id`),
  INDEX `FK_binding_binding` (`binding_id` ASC),
  CONSTRAINT `FK_binding_binding`
    FOREIGN KEY (`binding_id`)
    REFERENCES `final_task`.`binding` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_binding_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `final_task`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
