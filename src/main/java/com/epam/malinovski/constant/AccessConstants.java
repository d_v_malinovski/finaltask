package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class AccessConstants {
    public static final String ADMIN_ROLE = "admin";
    public static final String USER_ROLE = "user";
    public static final String GUEST_ROLE = "guest";
    public static final String ROLE = "role";
}
