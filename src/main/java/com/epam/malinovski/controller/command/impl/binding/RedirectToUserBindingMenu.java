package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * redirect to page with binding menu
 */
public class RedirectToUserBindingMenu implements Command {
    private static Logger logger = Logger.getLogger(RedirectToUserBindingMenu.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {

            int id = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);

            try {
                request.setAttribute(Attributes.BINDINGS, bindingService.getActiveBindings(id));
            } catch (ServiceException e) {
                logger.error(e.getMessage(), e);
            }
            return PagePathes.PATH_TO_BINDING_USER_MENU;
        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
