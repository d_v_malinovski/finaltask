package com.epam;

import com.epam.malinovski.util.PasswordManager;
import junit.framework.TestCase;

/**
 * @author Denis
 * Test for encrypt password
 */
public class EncryptionTest extends TestCase {

    private PasswordManager passwordManager = PasswordManager.getInstance();

    public void testEqualTruePassword() throws Exception {
        String incomingTruePassword = "jonny";
        String realPassword = "8f02d2977c5b09db2196714c1a986b966e5ca";
        assertTrue(passwordManager.comparePasswords(incomingTruePassword, realPassword));
    }

    public void testEqualFalsePassword() throws Exception {
        String incomingFalsePassword = "denis";
        String realPassword = "d59466ec001d91e6307543c0191bc14bdf34e";
        assertFalse(passwordManager.comparePasswords(incomingFalsePassword, realPassword));

    }

    public void testEqualEmptyPassword() throws Exception {
        String incomingEmptyPassword = "";
        String realPassword = "d59466ec007d91e6307543c0191bc14bdf34e";
        assertFalse(passwordManager.comparePasswords(incomingEmptyPassword, realPassword));
    }

}
