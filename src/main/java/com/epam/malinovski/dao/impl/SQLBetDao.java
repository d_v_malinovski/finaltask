package com.epam.malinovski.dao.impl;

import com.epam.malinovski.bean.Bet;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ExecuteQuery;
import com.epam.malinovski.dao.BetDao;
import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.dao.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * implements interface for <code>Bet</code> entity
 * @author Denis
 */
public class SQLBetDao implements BetDao {
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private SQLBetDao(){}

    public static BetDao getBetDao() {
        return SingletonHolder.betDao;
    }

    private static class SingletonHolder {
        private static final BetDao betDao = new SQLBetDao();
    }

    /**
     * method create new Bet
     * @param data information about new bet
     * @throws DaoException
     */
    @Override
    public void insert(Bet data) throws DaoException {
        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_BET)) {

            preparedStatement.setInt(1, data.getUserId());
            preparedStatement.setInt(2,data.getBindingId());
            preparedStatement.setTimestamp(3, data.getCurrentTime());
            preparedStatement.setBigDecimal(4,data.getBetAmount());

            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL, e);
        }

    }

    /**
     * Find last bet in current bindings
     * @param bindingId - current bindings serial number
     * @return serial number of bet owner
     * @throws DaoException
     */
    public int getLastBetFromCurrentBinding(int bindingId) throws DaoException{
        Bet bet;
        int userId = 0;

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_LAST_BET)) {

            preparedStatement.setInt(1,bindingId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                bet = new Bet();

                bet.setBetId(resultSet.getInt(DBFields.ID));
                bet.setUserId(resultSet.getInt(DBFields.BET_FROM_USER));
                bet.setBindingId(resultSet.getInt(DBFields.BET_TO_BINDING));
                bet.setCurrentTime(resultSet.getTimestamp(DBFields.BET_CURRENT_TIME));
                bet.setBetAmount(resultSet.getBigDecimal(DBFields.BET_AMOUNT));
                userId = bet.getUserId();

            }
        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return userId;
    }
}
