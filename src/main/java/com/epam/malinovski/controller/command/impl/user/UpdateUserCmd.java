package com.epam.malinovski.controller.command.impl.user;


import com.epam.malinovski.bean.User;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method update lot
 */
public class UpdateUserCmd implements Command {
    private static Logger logger = Logger.getLogger(UpdateUserCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();

    public String execute(HttpServletRequest request) {

        try {

            User current = new User();
            int id = Integer.parseInt(request.getParameter(Attributes.USER_ID));
            String login = request.getParameter(Attributes.LOGIN);
            String password = request.getParameter(Attributes.PASSWORD);
            int roleId = Integer.parseInt(request.getParameter(Attributes.ROLE));

            current.setId(id);
            current.setLogin(login);
            current.setPassword(password);
            current.setRoleId(roleId);

            userService.update(current);

        } catch (ServiceException | NumberFormatException e) {
            logger.error(e.getMessage(),e);
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
            return PagePathes.PATH_TO_UPDATE_USER_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_ADMIN_PAGE);
        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
