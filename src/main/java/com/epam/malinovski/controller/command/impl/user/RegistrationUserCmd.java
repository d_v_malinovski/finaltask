package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.bean.User;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.LangConstants;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Create new user operation
 */
public class RegistrationUserCmd implements Command {
    private static Logger logger = Logger.getLogger(AddUserCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();
    @Override
    public String execute(HttpServletRequest request) {
        try {

            User user = new User();

            String login = request.getParameter(Attributes.LOGIN);
            String password = request.getParameter(Attributes.PASSWORD);
            String language = request.getParameter(Attributes.LANGUAGE);

            user.setLogin(login);
            user.setPassword(password);
            user.setRoleId(Attributes.ROLE_USER);

            if(language.equals(Attributes.RUSSIAN)){
                user.setLanguage(LangConstants.RU);
            }
            if(language.equals(Attributes.ENGLISH)){
                user.setLanguage(LangConstants.EN);
            }

            userService.save(user);

        } catch (ServiceException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            Messager.sendMessage(request, Messages.USER_IS_ALREADY_EXIST);
            return PagePathes.PATH_TO_REGISTRATION_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_LOGIN_PAGE);

        return PagePathes.PATH_TO_LOGIN_PAGE;

    }
}
