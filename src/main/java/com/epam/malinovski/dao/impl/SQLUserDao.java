package com.epam.malinovski.dao.impl;

import com.epam.malinovski.bean.User;
import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ExecuteQuery;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.UserDao;
import com.epam.malinovski.util.PasswordManager;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>User</code> with the database
 */
public class SQLUserDao implements UserDao {
    private static final ConnectionPool pool = ConnectionPool.getInstance();
    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(100.00);

    private SQLUserDao(){

    }

    public static UserDao getUserDao(){
        return SingletonHolder.userDao;
    }

    private static class SingletonHolder{
        private static final UserDao userDao = new SQLUserDao();
    }

    /**
     * method to add new user
     * @param data includes information to addition
     * @throws DaoException
     */

    public void insert(User data) throws DaoException{
        PreparedStatement preparedStatement;
        Connection connection;
        PasswordManager passwordManager = PasswordManager.getInstance();

        try{
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_USER);

            preparedStatement.setString(1,data.getLogin());
            preparedStatement.setString(2, passwordManager.encryptWithMD5(data.getPassword()));
            preparedStatement.setInt(3, data.getRoleId());
            preparedStatement.setString(4, data.getLanguage());

            preparedStatement.executeUpdate();

            if(data.getRoleId() == Attributes.ROLE_USER) {

                preparedStatement = connection.prepareStatement(ExecuteQuery.SELECT_USER_BY_LOGIN);
                preparedStatement.setString(1, data.getLogin());
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    int id = resultSet.getInt(DBFields.ID);

                    preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_BILLING);
                    preparedStatement.setInt(1, id);
                    preparedStatement.setBigDecimal(2, DEFAULT_AMOUNT);

                    preparedStatement.executeUpdate();
                }
                resultSet.close();
            }

            preparedStatement.close();
            connection.close();

        }catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        }catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

    /**
     * method to update user
     * @param data includes information to update
     * @throws DaoException
     */

    public void update(User data) throws DaoException {

        PasswordManager passwordManager = PasswordManager.getInstance();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_USER_BY_ID)) {

            preparedStatement.setString(1,data.getLogin());
            preparedStatement.setString(2, passwordManager.encryptWithMD5(data.getPassword()));
            preparedStatement.setInt(3,data.getRoleId());
            preparedStatement.setInt(4,data.getId());
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

    /**
     * method to delete user by serial number
     * @param id serial number of the user to delete operation
     * @throws DaoException
     */

    public void deleteUserById(int id) throws DaoException{

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.DELETE_USER_BY_ID)) {

            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

    /**
     * method to get list of user
     * @param userId serial number current user witch user the application
     * @throws DaoException
     * @return list of users
     */

    public List<User> getAllUsers(int userId) throws DaoException{

        List<User> userList = new ArrayList<>();

        try(Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_ALL_USERS)) {

            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                User user = new User();

                user.setId(resultSet.getInt(DBFields.ID));
                user.setLogin(resultSet.getString(DBFields.LOGIN));
                user.setPassword(resultSet.getString(DBFields.PASSWORD));
                user.setRoleId(resultSet.getInt(DBFields.ROLE_ID));
                userList.add(user);
            }
        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return userList;
    }

    /**
     * method to find user by field login
     * @param login login current user witch use application
     * @throws DaoException
     * @return user with login from database is equal with init param
     */

    public User getUserByLogin(String login) throws DaoException{
        User user = null;

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.SELECT_USER_BY_LOGIN)) {

            preparedStatement.setString(1, login);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt(DBFields.ID));
                user.setLogin(resultSet.getString(DBFields.LOGIN));
                user.setPassword(resultSet.getString(DBFields.PASSWORD));
                user.setRoleId(resultSet.getInt(DBFields.ROLE_ID));
                user.setLanguage(resultSet.getString(DBFields.PREFORM_LANG));
            }
        }catch (SQLException ex){
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return user;

    }

    /**
     * method search user by field id and return information about this user
     * @param id serial number of user witch need to find
     * @return user
     * @throws DaoException
     */

    public User getUserById(int id) throws DaoException{
        User user = new User();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.SELECT_USER_BY_ID)){
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                user.setId(resultSet.getInt(DBFields.ID));
                user.setLogin(resultSet.getString(DBFields.LOGIN));
                user.setPassword(resultSet.getString(DBFields.PASSWORD));
                user.setRoleId(resultSet.getInt(DBFields.ROLE_ID));
            }

        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return user;
    }

    /**
     * method check user registration in current binding
     * @param userId - user serial number
     * @param bindingId - serial number of binding
     * @return true if user if member of binding or false if user does not a memeber
     * @throws DaoException
     */
    public boolean checkMemberShip(int userId, int bindingId) throws DaoException{
        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement
                    = connection.prepareStatement(ExecuteQuery.CHECK_USER_MEMBERSHIP_IN_BINDING)){

            preparedStatement.setInt(1,userId);
            preparedStatement.setInt(2,bindingId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next())
                return true;

        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return false;
    }

    /**
     * method init registration user in binding
     * @param userId - serial number of user
     * @param bindingId - serial number of binding
     * @throws DaoException
     */

    public void takePartInBinding(int userId, int bindingId) throws DaoException{

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.TAKE_PART_IN_BINDING)) {

            preparedStatement.setInt(1,userId);
            preparedStatement.setInt(2,bindingId);
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

    /**
     * method check vacation places in binding
     * @param bindingId - serial number of bindings
     * @return - return true if has free places
     * @throws DaoException
     */

    public boolean checkFreePositionForMemberShip(int bindingId)throws DaoException{
        try{

            Connection connection = pool.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.COMPUTE_COUNT_OF_PARTICIPANT);

            preparedStatement.setInt(1,bindingId);
            ResultSet resultSet = preparedStatement.executeQuery();

            int number_of_participant;
            int current_number_of_participant = 0;

            if(resultSet.next()){
                current_number_of_participant = resultSet.getInt(DBFields.BINDING_NUMBER_OF_PARTICIPANT);
            }

            preparedStatement = connection.prepareStatement(ExecuteQuery.GET_BINDING_BY_ID);
            preparedStatement.setInt(1,bindingId);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                number_of_participant = resultSet.getInt(DBFields.BINDING_NUMBER_OF_PARTICIPANT);

                if(current_number_of_participant < number_of_participant){
                    return true;
                }
            }

        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return false;
    }

}
