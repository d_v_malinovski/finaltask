package com.epam.malinovski.dao;

import com.epam.malinovski.dao.impl.SQLDaoFactory;

/**
 * @author Denis
 */
public abstract class DaoFactory {
    public static DaoFactory getDaoFactory(){
        return SQLDaoFactory.getInstance();
    }

    public abstract UserDao getUserDao();
    public abstract LotDao getLotDao();
    public abstract BindingDao getBindingDao();
    public abstract BillingDao getBillingDao();
    public abstract BetDao getBetDao();
    public abstract BasketDao getBasketDao();
}
