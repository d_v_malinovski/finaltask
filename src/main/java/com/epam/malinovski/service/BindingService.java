package com.epam.malinovski.service;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.bean.Lot;

import java.util.List;

/**
 * Created by Denis on 05.08.2017.
 */
public interface BindingService extends GenericService<Binding> {

    @Override
    default void save(Binding data) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    default List<Binding> showAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }

    List<Lot> getActiveBindings(int userId) throws ServiceException;
    void deleteBindingByID(int id) throws ServiceException;
    List<Lot> getBindingsWithOpenStateAndType(int statusId) throws ServiceException;
    void closeBinding(int bindingId, int userId, int lotId) throws ServiceException;
    void save(int lotId, Binding binding) throws ServiceException;
    List<Lot> getAllBindings() throws ServiceException;
    Binding getBindingById(int id) throws ServiceException;
}
