/**
 * Created by Denis on 01.08.2017.
 */

$(function () {
    $('#add_money_form').validate({
        rules:{
          money: {
              required: true,
              min:1
          },  
          bet:{
              required: true, 
              min:1
          }  
        },
        errorClass: 'has-error',
        onsubmit: true
    });
});
