/**
 * Created by Denis on 27.07.2017.
 */
$(function () {    
    $('#date_begin').datepicker({dateFormat: 'yy-mm-dd', changeYear: true, changeMonth:true,
        changeDay: true
        });

    $('#date_end').datepicker({ dateFormat: 'yy-mm-dd', changeYear: true, changeMonth:true,
        changeDay: true});

    $('#time_begin').timepicker({
        timeFormat: 'HH:mm:ss',
        interval: 60,
        minTime: '09:00:00',
        maxTime: '17:00:00',
        startTime: '09:00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#time_end').timepicker({
        timeFormat: 'HH:mm:ss',
        interval: 60,
        minTime: '09:00:00',
        maxTime: '17:00:00',
        startTime: '09:00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true

    });

});



