package com.epam.malinovski.controller.command.impl.payment;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BasketService;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BasketServiceImpl;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Method abort current payment
 */
public class AbortPayCmd implements Command {
    private static Logger logger = Logger.getLogger(AbortPayCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    private static final BasketService basketService = BasketServiceImpl.getBasketService();

    @Override
    public String execute(HttpServletRequest request) {
        int basketId = Integer.parseInt(request.getParameter(Attributes.BASKET_ID));
        int lotId = Integer.parseInt(request.getParameter(Attributes.LOT_ID));

        Lot lot;

        try {

            lot = lotService.getLotById(lotId);
            basketService.deletePaymentById(basketId);
            lotService.changeLotState(lotId, DBFields.LOT_ABORTED_SALE);
            bindingService.deleteBindingByID(lot.getBinding().getId());

        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
            return PagePathes.PATH_TO_PAYMENT_LIST;
        }

        return PagePathes.PATH_TO_USER_PAGE;
    }
}
