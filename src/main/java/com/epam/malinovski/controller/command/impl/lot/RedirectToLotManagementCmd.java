package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method redirect to managment los
 */
public class RedirectToLotManagementCmd implements Command {
    private static Logger logger = Logger.getLogger(RedirectToLotManagementCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {
            int id = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);
            try {
                request.setAttribute(Attributes.LOT_LIST, lotService.getAllLotByUserId(id));
            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
            }
            return PagePathes.PATH_TO_LOT_PAGE;
        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
