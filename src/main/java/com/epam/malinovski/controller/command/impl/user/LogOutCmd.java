package com.epam.malinovski.controller.command.impl.user;


import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Denis
 * This method for extit from application
 */
public class LogOutCmd implements Command {

    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session != null) {
            session.removeAttribute(Attributes.USER);
            session.removeAttribute(Attributes.CURRENT_USER_ID);
            session.removeAttribute(AccessConstants.ROLE);
            session.removeAttribute(Attributes.ROLE);
        }
        return PagePathes.PATH_TO_LOGIN_PAGE;
    }
}
