package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class ExceptionMessage {
    public static final String SQL_FAIL = "Sql cannot be execute";
    public static final String CONNECTION_FAIL = "Connection fail";
    public static final String DRIVER_NOT_FOUND = "driver not found";
    public static final String INIT_FAIL = "initialization of database fall";
    public static final String INTERRUPTED = "Interrupted at moment when taking connectionpool";
    public static final String CANT_GET_FREE_CONNECTION = "Cannot get free connectionpool ";
    public static final String FREE_CONNECTION_ERROR = "Tried to close non pooled connectionpool ";
    public static final String CP_CLOSE_FAIL = "Connection pool close fail";
}
