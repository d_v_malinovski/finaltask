package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method get page for lot update
 */
public class RedirectToUpdateLot implements Command {
    private static Logger logger = Logger.getLogger(RedirectToUpdateLot.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {
            int id = Integer.parseInt(request.getParameter(Attributes.LOT_ID));
            Lot lot = null;

            try {
                lot = lotService.getLotById(id);
            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
            }
            request.getSession().setAttribute(Attributes.LOT,lot);
            return PagePathes.PATH_TO_UPDATE_LOT;
        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
