package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class ResourcesPathes{
    public static final String DB_RESOURCE_LOCATION = "db";

    public static final String EN_MESSAGES_RESOURCE_LOCATION = "messages_en_US";
    public static final String RU_MESSAGES_RESOURCE_LOCATION = "messages_ru_RU";
    public static final String DEFAULT_MESSAGES_RESOURCE_LOCATION = "messages";
}

