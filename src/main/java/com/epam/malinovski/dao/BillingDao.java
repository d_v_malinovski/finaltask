package com.epam.malinovski.dao;

import com.epam.malinovski.bean.Billing;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Denis
 */
public interface BillingDao extends GenericDao<Billing> {
    @Override
    default void insert(Billing data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default void update(Billing data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default void delete(Billing data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default List<Billing> select(Billing data) throws DaoException, SQLException {
        throw new UnsupportedOperationException();
    }

    void createMoneyExchange(int newOwnerId, int oldOwnerBillingId, BigDecimal amount) throws DaoException;
    void setMoneyAmountToBillingById(int billingById, BigDecimal moneyAmount) throws DaoException;
    Billing getBillingByUserId(int userId)throws DaoException;
    Billing getBillingById(int billingId) throws DaoException;
    void addBilling(int userId, BigDecimal money) throws DaoException;
}
