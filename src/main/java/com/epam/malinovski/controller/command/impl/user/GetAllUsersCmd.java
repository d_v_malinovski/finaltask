package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Get all user list command
 */
public class GetAllUsersCmd implements Command {
    private static Logger logger = Logger.getLogger(GetAllUsersCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();

    public String execute(HttpServletRequest request) {
        try {

            int userId = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);

            request.setAttribute(Attributes.USERS_LIST, userService.getAll(userId));

            } catch (ServiceException e) {
            logger.error(e.getMessage(), e);
        }

        return PagePathes.PATH_TO_MANAGEMENT_USERS;
    }
}
