package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class Attributes{
    public static final String ROLE_ID = "role";

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";

    public static final String PAGE = "page";
    public static final String MESSAGE = "message";

    public static final String USERS_LIST = "userList";
    public static final String BILLING_LIST = "billingList";
    public static final String LOT_LIST = "lotList";

    public static final String LOT_ID = "lotId";
    public static final String BINDINGS = "bindingList";
    public static final String PAYMENTS = "paymentList";

    public static final String ADMIN = "admin";
    public static final String USER = "user";

    public static final String RUSSIAN = "ru";
    public static final String ENGLISH = "en";
    public static final String CURRENT_USER_ID = "current_id";

    public static final int ROLE_ADMIN = 2;
    public static final int ROLE_USER = 1;

    public static final String LANGUAGE = "preform_lang";
    public static final String FIELD_USER_ID = "userId";

    public static String PRICE = "price";
    public static String LOT_NAME = "lot_name";
    public static String LOT_DESCRIPTION = "description";
    public static String LOT_STATUS = "status";
    public static String CURRENT_LOCALE = "current_locale";
    public static String BASKET_ID = "basketId";
    public static String BET = "bet";
    public static String BLITZ = "blitz";
    public static String STEP = "step";
    public static String BINDING_ID = "bindingId";
    public static String START = "start";
    public static String END = "end";
    public static String VARIANT_LOT_ID = "lot_id";
    public static String END_PRICE = "end_price";
    public static String DATE_BEGIN = "date_begin";
    public static String DATE_END = "date_end";
    public static String TIME_BEGIN = "time_begin";
    public static String TIME_END = "time_end";
    public static String NUMBER_PARTICIPANT = "number_of_participant";
    public static String BLITZ_PRICE = "blitz_price";
    public static String BINDING_STEP = "binding_step";
    public static String LOT = "lot";
    public static int NULL_VALUE = 0;
    public static String NEW_OWNER_ID = "newOwnerId";
    public static String OLD_OWNER_BILLING = "ownerBillingId";
    public static String BILLING_ID = "billingID";
    public static String MONEY_AMOUNT = "money";
    public static String USER_ID = "user_id";
    public static String BINDING_TYPE = "binding_type";

    public static String SHORT_NAME_INTERNET_AUCTION = "in";
    public static String SHORT_NAME_BLITZ_AUCTION = "bl" ;
    public static String SHORT_NAME_ENGLISH_AUCTION = "en";
    public static String BILLING_STATE = "billingState";

}
