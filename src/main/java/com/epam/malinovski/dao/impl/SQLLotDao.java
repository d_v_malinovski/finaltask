package com.epam.malinovski.dao.impl;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.bean.User;
import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ExecuteQuery;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.LotDao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * implement interface DAO for entity <code>Lot</code>
 * @author Denis
 */
public class SQLLotDao implements LotDao {
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private SQLLotDao(){

    }
    private static class SingletonHolder{
        private static final LotDao lotDao = new SQLLotDao();
    }
    public static LotDao getLotDao(){
        return SingletonHolder.lotDao;
    }
    /**
     * update information about current lot entity
     * @param data - new lot entity
     * @throws DaoException
     */
    @Override
    public void insert(Lot data) throws DaoException {

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_LOT)) {

            preparedStatement.setInt(1,data.getState());
            preparedStatement.setInt(2, data.getUser().getId());
            preparedStatement.setString(3, data.getName());
            preparedStatement.setBigDecimal(4,data.getPrice());
            preparedStatement.setString(5,data.getDescription());

            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        }catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
    @Override
    public void update(Lot data) throws DaoException {

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_LOT)) {

            preparedStatement.setBigDecimal(1,data.getPrice());
            preparedStatement.setString(2, data.getName());
            preparedStatement.setString(3,data.getDescription());
            preparedStatement.setInt(4,data.getState());
            preparedStatement.setInt(5,data.getLotId());
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
    /**
     * change lot owner when lot was buy to other user
     * @param lotId - serial lot number
     * @param userId - serial number of new owner
     * @throws DaoException
     */
    public void updateLotOwner(int lotId, int userId) throws DaoException{
        try( Connection connection = pool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_LOT_OWNER)) {

            preparedStatement.setInt(1,userId);
            preparedStatement.setInt(2,lotId);

            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
    /**
     * method get information about all lots of current user
     * @param userId current user id
     * @return list of lots
     * @throws DaoException
     */
    public List<Lot> getAllLotByUserId(int userId) throws DaoException{
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List<Lot> lots = new ArrayList<>();
        ResultSet resultSet = null;

        try {

            connection = pool.getConnection();


            preparedStatement = connection.prepareStatement(ExecuteQuery.SELECT_USER_BY_ID);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                User user = new User();

                user.setId(resultSet.getInt(DBFields.ID));
                user.setLogin(resultSet.getString(DBFields.LOGIN));
                user.setPassword(resultSet.getString(DBFields.PASSWORD));
                user.setRoleId(resultSet.getInt(DBFields.ROLE_ID));

                preparedStatement = connection.prepareStatement(ExecuteQuery.SELECT_USERS_LOTS);
                preparedStatement.setInt(1, userId);
                resultSet = preparedStatement.executeQuery();


                while (resultSet.next()){
                    Lot lot = new Lot();

                    lot.setUser(user);
                    lot.setLotId(resultSet.getInt(DBFields.ID));
                    lot.setState(resultSet.getInt(DBFields.LOT_STATE));
                    lot.setName(resultSet.getString(DBFields.LOT_NAME));
                    lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));
                    lot.setDescription(resultSet.getString(DBFields.LOT_DESCRIPTION));


                    lots.add(lot);
                }
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return lots;
    }
    public Lot getLotById(int lotId) throws DaoException{
        Lot lot = new Lot();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_LOT_BY_ID)){

            preparedStatement.setInt(1,lotId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                lot.setLotId(resultSet.getInt(DBFields.ID));
                lot.setState(resultSet.getInt(DBFields.LOT_STATE));
                lot.setName(resultSet.getString(DBFields.LOT_NAME));
                lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));
                lot.setDescription(resultSet.getString(DBFields.LOT_DESCRIPTION));
                Binding binding = new Binding();
                binding.setId(resultSet.getInt(DBFields.BINDING_ID));
                lot.setBinding(binding);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return lot;
    }
    /**
     * change current state to new state for lot
     * @param lotId - lot id with state will be change
     * @param stateId - new state of lot
     * @throws DaoException
     */
    public void changeLotState(int lotId, int stateId) throws DaoException{
        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.CHANGE_LOT_STATE)){


            preparedStatement.setInt(1,stateId);
            preparedStatement.setInt(2,lotId);

            preparedStatement.executeUpdate();

        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        }
    }
    /**
     * method return list of lots witch owners want to sold them
     * @return list of lots
     * @throws DaoException
     */
    public List<Lot> getLotForSale() throws DaoException{

        List<Lot> lots = new ArrayList<>();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_ALL_LOT_FOR_SALE)) {


            preparedStatement.setInt(1,DBFields.LOT_STATE_FOR_SALE);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){

                    Lot lot = new Lot();
                    User user = new User();
                    user.setId(resultSet.getInt(DBFields.OWNER));
                    user.setLogin(resultSet.getString(DBFields.LOGIN));

                    lot.setUser(user);

                    lot.setLotId(resultSet.getInt(DBFields.ID));
                    lot.setState(resultSet.getInt(DBFields.LOT_STATE));
                    lot.setName(resultSet.getString(DBFields.LOT_NAME));
                    lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));
                    lot.setDescription(resultSet.getString(DBFields.LOT_DESCRIPTION));

                    lots.add(lot);
            }

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return lots;
    }
    /**
     * method change current price of lot
     * @param lotId lot id witch price will be change
     * @param price - new price of lot
     * @throws DaoException
     */
    public void updatePriceLotById(int lotId, BigDecimal price) throws DaoException{

        try(Connection connection = pool.getConnection();
            PreparedStatement  preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_PRICE_OF_LOT)){

            preparedStatement.setBigDecimal(1,price);
            preparedStatement.setInt(2,lotId);
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
}
