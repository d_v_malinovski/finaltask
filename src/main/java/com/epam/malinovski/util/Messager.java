package com.epam.malinovski.util;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.LangConstants;
import com.epam.malinovski.constant.ResourcesPathes;

import javax.servlet.http.HttpServletRequest;
import java.util.ResourceBundle;

/**
 * @author Denis
 * This class load message with current key from locale.properties
 */
public class Messager {
    public static void sendMessage(HttpServletRequest request, String message) {
        String lang = request.getSession().getAttribute(LangConstants.BUNDLE).toString();
        ResourceBundle bundle = ResourceBundle.getBundle(ResourcesPathes.DEFAULT_MESSAGES_RESOURCE_LOCATION);

        if (lang.equals(LangConstants.EN)) {
            bundle = ResourceBundle.getBundle(ResourcesPathes.EN_MESSAGES_RESOURCE_LOCATION);
        }
        if (lang.equals(LangConstants.RU)) {
            bundle = ResourceBundle.getBundle(ResourcesPathes.RU_MESSAGES_RESOURCE_LOCATION);
        }
        request.setAttribute(Attributes.MESSAGE, bundle.getString(message));

    }
}
