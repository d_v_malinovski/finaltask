<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 10.07.2017
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib uri="/WEB-INF/mytag.tld" prefix="m" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <fmt:setBundle basename="locale"/>
    <title><fmt:message key="label.users"/> </title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">
            <div class="navbar-form navbar-left">
                <div class="form-group">
                    <input class="form-control" readonly="true" value="<m:today/>">
                </div>
            </div>
        </div>

        <div class="collapse navbar-collapse">

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.users"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToUsersManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.bindings"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToBindingManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control"  type="submit" value="<fmt:message key="label.add.user"/> "/>
                </div>
                <input type="hidden" name="cmd" value="redirectToAddUserCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.show.all.users"/> ">
                </div>
                <input type="hidden" name="cmd" value="getUserList">
            </form>

            <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
                <input type="hidden" name="cmd" value="changeLangCmd">
                <input type="hidden" name="path" value="html/showPages/managementClient.jsp">

                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="ru">
                </div>
                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="en">
                </div>
            </form>

            <%-- Выйти из приложения --%>
            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.log.out"/>"/>
                </div>
                <input type="hidden" name="cmd" value="logOutCmd">
            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
<c:if test="${not empty userList}">
<table>
    <caption class="text-center">
      <label class="h2"><fmt:message key="label.table.user.header"/></label>
    </caption>
    <thead>
    <tr class="text-center">
        <th class="text-center" ><fmt:message key="label.login"/> </th>
        <th class="text-center"><fmt:message key="label.password"/> </th>
        <th class="text-center"><fmt:message key="label.role"/></th>
    </tr>
    </thead>
    <c:forEach var="user" items="${userList}">
        <tbody >
        <tr class="text-center">
            <td class="col-xs-3"><c:out value="${user.login}"/></td>
            <td class="col-xs-3"><c:out value="${user.password}"/></td>
            <td class="col-xs-3">
            <c:choose>
                <c:when test="${user.roleId eq 1}">
                    <fmt:message key="label.user"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="label.admin"/>
                </c:otherwise>
            </c:choose>
            </td>
            <td class="col-xs-3 text-left" colspan="2">
                <form method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control btn btn-success navbar-btn" type="submit" value="<fmt:message key="label.update.user"/>" >
                </div>
                    <input type="hidden" name="userId" value="${user.id}">
                <input type="hidden" name="cmd" value="redirectToUpdateUserCmd">
            </form>
            </td>
            <td class="col-xs-2" class="text-right">
                <form class="form-control-static" method="get" action="${pageContext.request.contextPath}/Control">
                    <div class="form-group">
                    <input class="form-control btn btn-danger navbar-btn" type="submit" value="<fmt:message key="label.delete.user"/> ">
                    </div>
                    <input type="hidden" name="userId" value="${user.id}">
                    <input type="hidden" name="cmd" value="deleteUserCmd">
                </form>
            </td>
        </tr>
        </tbody>
    </c:forEach>
</c:if>
</table>
</div>
</body>
</html>
