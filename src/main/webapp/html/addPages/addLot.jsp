<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 10.07.2017
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <fmt:setBundle basename="locale" scope="session"/>
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <title><fmt:message key="label.add.lot"/> </title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru}">
    <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>
    <script src="${pageContext.request.contextPath}/js/addLotValidation.js"></script>


</head>
<body>
<div class="container">
    <div class="col-md-4 col-md-offset-4">
<form id="add_lot_form" class="form-signin" method="post" action="${pageContext.request.contextPath}/Control">
    <p id="message" class="has-error">${message}</p>
    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="lot.name"/></label></h2>
    <div class="form-group">
        <input class="form-control" required minlength="5" type="text" name="lot_name" placeholder="<fmt:message key="lot.name"/> " id="lot_name">
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="lot.description"/></label></h2>
    <div class="form-group">
        <textarea maxlength ="255" required minlength="5" cols="20" wrap="soft"  class="form-control" type="text" name="description" placeholder="<fmt:message key="lot.description"/> " id="description"></textarea>
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="lot.price"/></label></h2>
    <div class="form-group">
        <input class="form-control" required min="1" type="text" name="price" placeholder="<fmt:message key="lot.price"/> " id="price">
    </div>

    <input class="btn btn-success" type="submit" value="<fmt:message key="label.add.lot"/> ">
    <input type="hidden" name="cmd" value="addLotCmd">
</form>
</div>

    <div class="col-md-4 col-md-offset-4">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelCmd">
        </form>
    </div>
</div>
</body>
</html>
