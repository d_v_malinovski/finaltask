package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method need to abort auction
 */
public class FilmedBindingCmd implements Command {
    private static Logger logger = Logger.getLogger(FilmedBindingCmd.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {

        int bindingId = Integer.parseInt(request.getParameter(Attributes.BINDING_ID));
        int lotId = Integer.parseInt(request.getParameter(Attributes.VARIANT_LOT_ID));
        int state = DBFields.LOT_ABORTED_SALE;


        try {
            lotService.changeLotState(lotId,state);
            bindingService.deleteBindingByID(bindingId);
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
            return PagePathes.PATH_TO_BINDING_PAGE;
        }
        return PagePathes.PATH_TO_BINDING_PAGE;
    }
}
