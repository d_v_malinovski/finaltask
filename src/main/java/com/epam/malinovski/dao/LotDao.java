package com.epam.malinovski.dao;

import com.epam.malinovski.bean.Lot;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * @author  Denis on 05.08.2017.
 */
public interface LotDao extends GenericDao<Lot> {

    @Override
    default void delete(Lot data) throws DaoException {
        throw new UnsupportedOperationException();
    }
    @Override
    default List<Lot> select(Lot data) throws DaoException, SQLException {
        throw new UnsupportedOperationException();
    }
    void updateLotOwner(int lotId, int userId) throws DaoException;
    List<Lot> getAllLotByUserId(int userId) throws DaoException;
    Lot getLotById(int lotId) throws DaoException;
    void changeLotState(int lotId, int stateId) throws DaoException;
    List<Lot> getLotForSale() throws DaoException;
    void updatePriceLotById(int lotId, BigDecimal price) throws DaoException;
}
