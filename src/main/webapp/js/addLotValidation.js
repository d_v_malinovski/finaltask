/**
 * Created by Denis on 01.08.2017.
 */
$(function () {

    $.validator.addMethod('regexp', function(value, element, params) {
        var expression = new RegExp(params);
        return this.optional(element) || expression.test(value);
    });


    $('#add_lot_form').validate({
         rules:{
             lot_name:{
                 required: true,                 
                 minlength: 5,
                 regexp: /^[a-zA-Zа-яА-Я0-9 \._-]{4,50}/
             },           
             description: {
                 required:true,
                 minlength: 5,
                 maxlength: 255
             },
             price : {
                 required: true,
                 min: 1
             }

         },
        errorClass: 'has-error',
        onsubmit: true
    });
});
