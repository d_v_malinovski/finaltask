package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * method for deleting user
 */
public class DeleteUserCmd implements Command {
    private static Logger logger = Logger.getLogger(DeleteUserCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();
    @Override
    public String execute(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter(Attributes.FIELD_USER_ID));
        try {
            userService.delete(id);
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
            return PagePathes.PATH_TO_ERROR_PAGE;
        }
        return PagePathes.PATH_TO_MANAGEMENT_USERS;
    }
}
