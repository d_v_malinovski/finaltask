package com.epam.malinovski.bean;

import java.math.BigDecimal;

/**
 * @author Denis
 */
public class Billing {
    private int billingId;
    private User user;
    private BigDecimal moneyAmount;

    public Billing(){

    }

    public Billing(int billingId, User user, BigDecimal moneyAmount){
        this.billingId = billingId;
        this.user = user;
        this.moneyAmount = moneyAmount;
    }

    public int getBillingId() {
        return billingId;
    }

    public void setBillingId(int billingId) {
        this.billingId = billingId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    @Override
    public String toString() {
        return "Billing{" +
                "billingId=" + billingId +
                ", user=" + user +
                ", moneyAmount=" + moneyAmount +
                '}';
    }
}
