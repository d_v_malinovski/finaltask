package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Get all lots for sale
 */
public class GetLotsForSaleCmd implements Command {
    private static Logger logger = Logger.getLogger(GetLotsForSaleCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {
        try {
            request.setAttribute(Attributes.LOT_LIST, lotService.getLotForSale());
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
        }
        return PagePathes.PATH_TO_BINDING_PAGE;
    }
}
