package com.epam.malinovski.controller.command;

import javax.servlet.http.HttpServletRequest;

/**
 * interface for Command pattern
 * @author Denis
 */
public interface Command {
    /**
     * @param request servlet request where/from put/get data
     * @return Page path string, which define where will bee request and response forwarded or redirected
     */
    String execute (HttpServletRequest request);
}
