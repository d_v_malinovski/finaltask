<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 10.07.2017
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<html>
<head>
    <fmt:setBundle basename="locale"/>
    <title><fmt:message key="label.bindings"/> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">
        </div>


        <div class="collapse navbar-collapse">

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.users"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToUsersManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.bindings"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToBindingManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.show.lots"/> ">
                </div>
                <input type="hidden" name="cmd" value="GetOffersLotCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.show.bindings"/> ">
                </div>
                <input type="hidden" name="cmd" value="getAllBindingCmd">
            </form>

            <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
                <input type="hidden" name="cmd" value="changeLangCmd">
                <input type="hidden" name="path" value="html/showPages/bindingPage.jsp">

                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="ru">
                </div>
                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="en">
                </div>
            </form>

            <%-- Выйти из приложения --%>
            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.log.out"/>"/>
                </div>
                <input type="hidden" name="cmd" value="logOutCmd">
            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
<c:if test="${not empty lotList}">
<table class="table">

    <caption class="text-center">
        <label class="h2"><fmt:message key="label.lots.for.bindings"/></label>
    </caption>

    <c:forEach var="lot" items="${lotList}">

        <tr class="text-center">
            <th class="col-xs-3"><fmt:message key="lot.name"/></th>
            <td><c:out value="${lot.name}"/></td>
        </tr>

        <tr class="text-center">
            <th class="col-xs-3"><fmt:message key="lot.description"/></th>
            <td><c:out value="${lot.description}"/></td>
        </tr>

        <tr class="text-center">
            <th class="col-xs-3"><fmt:message key="lot.price"/></th>
            <td><c:out value="${lot.price}  $"/></td>
        </tr>


    <tr class ="text-center">
        <td rowspan="2" class="col-md-4  col-xs-4 text-center" >
            <form  method="get" action="${pageContext.request.contextPath}/Control">
                <input type="hidden" name="lotId" value="${lot.lotId}">
                <br>
                <input class = "btn btn-danger navbar-btn" type="submit" value="<fmt:message key="label.blocking.lot"/> ">
                <input type="hidden" name="cmd" value="blockingLotCmd">
            </form>
        </td>
    </tr>

    <tr>
      <td class="col-md-4 col-xs-4 text-center" >
                <form class="form-horizontal" method="get" action="${pageContext.request.contextPath}/Control">

                    <input class="bottom-left"  name="binding_type" type="radio" value="in"><fmt:message key="internet.binding"/>
                    <input  name="binding_type" type="radio" value="bl"><fmt:message key="blitz.binding"/>
                    <input  name="binding_type" type="radio" value="en" checked><fmt:message key="english.binding"/>


                    <div class="text-center form-group">
                        <input class = "btn btn-success navbar-btn" type="submit" value="<fmt:message key="label.add.bindings"/> ">
                    </div>

                    <input type="hidden" name="lotId" value="${lot.lotId}">

                    <input type="hidden" name="cmd" value="redirectToAddBindingCmd">

                </form>
        </td>
    </tr>
    </c:forEach>
</table>
</c:if>
</div>


<div class="container-fluid">
<c:if test="${not empty bindingList}">
<table class="table">

    <caption class="text-center">
        <label class="h2"><fmt:message key="label.bindings"/></label>
    </caption>

    <c:forEach var="lot" items="${bindingList}">
    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="lot.name"/></th>
        <td><c:out value="${lot.name}"/></td>
    </tr>

    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="lot.description"/></th>
        <c:set var="description" value="${lot.description}"/>
        <td><textarea readonly="true" class="form-control text-center">${description}</textarea></td>
    </tr>

    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="lot.price"/> </th>
        <td><c:out value="${lot.price}   $"/></td>
    </tr>

    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="binding.state"/></th>
        <td>
        <c:choose>
            <c:when test="${lot.binding.state eq 1}">
                <fmt:message key="label.open"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="label.close"/>
            </c:otherwise>
        </c:choose>
        </td>
    </tr>

    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="binding.type"/></th>
        <td>
            <c:choose>
                <c:when test="${lot.binding.type eq 1}">
                    <fmt:message key="english.binding"/>
                </c:when>
                <c:when test="${lot.binding.type eq 2}">
                    <fmt:message key="blitz.binding"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="internet.binding"/>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>

    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="binding.start"/></th>
        <c:set var="start" value="${lot.binding.start}"/>
        <td><fmt:formatDate type = "both" dateStyle = "long" timeStyle = "long"  value = "${start}"/></td>
    </tr>

    <tr class="text-center">
        <th class="col-xs-3"><fmt:message key="binding.end"/></th>
        <c:set var="end" value="${lot.binding.end}"/>
        <td><fmt:formatDate type = "both" dateStyle = "long" timeStyle = "long"  value = "${end}"/></td>
    </tr>


    <tr class="text-center">

        <td>
          <form method="get" action="${pageContext.request.contextPath}/Control">
              <div class="form-group">
                    <input class="form-control btn btn-primary" type="submit" value="<fmt:message key="label.filmed.binding"/> ">
              </div>
                    <input type="hidden" name="cmd" value="filmedBindingCmd">
                    <input type="hidden" name="lot_id" value="<c:out value="${lot.lotId}"/>">
                    <input type="hidden" name="bindingId" value="<c:out value="${lot.binding.id}"/>">
          </form>
        </td>

    </tr>

    </c:forEach>
</table>
</c:if>
</div>
</body>
</html>
