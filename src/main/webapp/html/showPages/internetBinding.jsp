<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 21.07.2017
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session" />
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <title><fmt:message key="internet.binding"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru }">
        <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>

    <script src="${pageContext.request.contextPath}/js/moneyFormValidation.js"></script>

</head>
<body>
<div class="container-fluid">
    <table class="table">
        <caption class=" caption text-center">

        </caption>

        <tr class="text-center">
            <th><fmt:message key="binding.start"/> </th>
            <td><c:out value = "${lot.binding.start}"/></td>
        </tr>

        <tr class="text-center">
            <th class="col-xs-3"><fmt:message key="binding.end"/></th>
            <td><c:out  value = "${lot.binding.end}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="binding.type"/> </th>
            <td><fmt:message key="internet.binding"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="lot.name"/> </th>
            <td><c:out value="${lot.name}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="lot.price"/> </th>
            <td><c:out value="${lot.price}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="lot.description"/> </th>
            <td><textarea maxlength ="255" cols="20" wrap="soft" class="form-control text-center" readonly="true"><c:out value="${lot.description}"/></textarea></td>
        </tr>
    </table>
<div class="container">
    <div class="col-md-4 col-md-offset-4">
<form id="add_money_form" class="form-signin" method="post" action="${pageContext.request.contextPath}/Control">
    <p id="message" class="has-error">${message}</p>
    <h5 class="form-signin-heading"><label class="control-label"><fmt:message key="bet.amount"/></label></h5>
    <div class="form-group">
    <input class="form-control" required min="1" type="text" placeholder="<fmt:message key="bet.amount"/> " name="bet" id="bet">
    </div>
    <input class="btn btn-success" type="submit" value="<fmt:message key="label.add.bet"/>">
    <input type="hidden" name="cmd" value="addBetForInternetBindingCmd">
    <input type="hidden" name="lot_id" value="<c:out value="${lot.lotId}"/>">
    <input type="hidden" name="bindingId" value="<c:out value="${lot.binding.id}"/>">
    <input type="hidden" name="start" value="<c:out value="${lot.binding.start}"/>">
    <input type="hidden" name="end" value="<c:out value="${lot.binding.end}"/>">
</form>
        </div>
    <div class="col-md-4 col-md-offset-4">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelCmd">
        </form>
    </div>
</div>
</div>
</body>
</html>
