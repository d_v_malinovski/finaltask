package com.epam.malinovski.controller.command.impl.payment;

import com.epam.malinovski.bean.Billing;
import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.*;
import com.epam.malinovski.service.impl.BasketServiceImpl;
import com.epam.malinovski.service.impl.BillingServiceImpl;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Metod need to pay product from auction
 */
public class PayLotCmd implements Command {
    private static Logger logger = Logger.getLogger(PayLotCmd.class);
    private static final BasketService basketService = BasketServiceImpl.getBasketService();
    private static final LotService lotService = LotServiceImpl.getLotService();
    private static final BillingService billingService = BillingServiceImpl.getBillingService();
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    @Override
    public String execute(HttpServletRequest request) {

        int basketId = Integer.parseInt(request.getParameter(Attributes.BASKET_ID));
        int lotId = Integer.parseInt(request.getParameter(Attributes.LOT_ID));
        int newOwnerId = Integer.parseInt(request.getParameter(Attributes.NEW_OWNER_ID));
        int oldOwnerBilling = Integer.parseInt(request.getParameter(Attributes.OLD_OWNER_BILLING));

        Lot lot;
        Billing userBilling;

        try {
            lot = lotService.getLotById(lotId);
            userBilling = billingService.getBillingById(newOwnerId);

            int result = userBilling.getMoneyAmount().compareTo(lot.getPrice());

            if(result == -1){
                Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
                return PagePathes.PATH_TO_PAYMENT_LIST;
            }

            billingService.createMoneyExchange(newOwnerId, oldOwnerBilling, lot.getPrice());
            lotService.updateLotOwner(lotId, newOwnerId);
            basketService.deletePaymentById(basketId);
            lotService.changeLotState(lotId, DBFields.LOT_NOT_FOR_SALE);
            bindingService.deleteBindingByID(lot.getBinding().getId());

        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
            return PagePathes.PATH_TO_PAYMENT_LIST;
        }
        return PagePathes.PATH_TO_USER_PAGE;
    }
}
