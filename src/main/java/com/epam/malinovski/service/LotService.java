package com.epam.malinovski.service;

import com.epam.malinovski.bean.Lot;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Denis
 */
public interface LotService extends GenericService<Lot> {
    @Override
    default List<Lot> showAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }
    void updateLotOwner(int lotId, int userId) throws ServiceException;
    List<Lot> getAllLotByUserId(int userId) throws ServiceException;
    Lot getLotById(int lotId) throws ServiceException;
    void changeLotState(int lotId, int stateId) throws ServiceException;
    List<Lot> getLotForSale() throws ServiceException;
    void updatePriceLotById(int lotId, BigDecimal price) throws ServiceException;
    void update(Lot data) throws ServiceException;
}
