package com.epam.malinovski.listener;


import com.epam.malinovski.constant.LangConstants;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author Denis
 * This class maintance the session object
 */
public class ManagerSessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
       httpSessionEvent.getSession().setAttribute(LangConstants.BUNDLE, LangConstants.RU);
    }


    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
