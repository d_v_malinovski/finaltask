package com.epam.malinovski.controller.command.impl.payment;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BasketService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BasketServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * get All payments for product from auction
 */
public class GetPaymentListCmd implements Command {
    private static Logger logger = Logger.getLogger(GetPaymentListCmd.class);
    private static final BasketService basketService = BasketServiceImpl.getBasketService();
    @Override
    public String execute(HttpServletRequest request) {
        int userId = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);
    try {
         request.setAttribute(Attributes.PAYMENTS, basketService.getAllProductToPayment(userId));
            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
            }
            return PagePathes.PATH_TO_PAYMENT_LIST;
    }

}
