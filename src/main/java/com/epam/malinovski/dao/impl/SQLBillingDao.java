package com.epam.malinovski.dao.impl;

import com.epam.malinovski.bean.Billing;
import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ExecuteQuery;
import com.epam.malinovski.dao.BillingDao;
import com.epam.malinovski.dao.DaoException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * implements interface DAO for <code>Billing</code>
 * @author Denis
 */
public class SQLBillingDao implements BillingDao {
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private SQLBillingDao(){

    }
    private static class SingletonHolder{
        private static final BillingDao billingDao = new SQLBillingDao();
    }
    public static BillingDao getBillingDao(){
        return SingletonHolder.billingDao;
    }
    /**
     * sent money from first billing to other billing
     * @param newOwnerId -  serial number of user with want to create money exchange
     * @param oldOwnerBilling - serial number of billing other user
     * @param amount - money amount for exchange
     * @throws DaoException
     */
    public void createMoneyExchange(int newOwnerId, int oldOwnerBilling, BigDecimal amount) throws DaoException{
        Connection connection;
        int newOwnerBillingId = 0;

        try {
            connection = pool.getConnection();
            PreparedStatement  preparedStatement = connection.prepareStatement(ExecuteQuery.GET_BILLING_BY_USER_ID);
            preparedStatement.setInt(1,newOwnerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                newOwnerBillingId = resultSet.getInt(DBFields.ID);
            }

            preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_MONEY_TO_BILLING);
            preparedStatement.setBigDecimal(1,amount);
            preparedStatement.setInt(2,oldOwnerBilling);

            preparedStatement.executeUpdate();


            preparedStatement = connection.prepareStatement(ExecuteQuery.SUBTRACT_MONEY_TO_BILLING);
            preparedStatement.setBigDecimal(1,amount);
            preparedStatement.setInt(2,newOwnerBillingId);

            preparedStatement.executeUpdate();

            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
    /**
     * method sent money to current billing
     * @param billingById - current billing
     * @param moneyAmount - amount of money
     * @throws DaoException
     */
    public void setMoneyAmountToBillingById(int billingById, BigDecimal moneyAmount) throws DaoException{

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement =
                        connection.prepareStatement(ExecuteQuery.SEND_CURRENT_SUM_TO_BILLING_BY_ID)) {

            preparedStatement.setBigDecimal(1, moneyAmount);
            preparedStatement.setInt(2, billingById);
            preparedStatement.executeUpdate();

        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        }
    }
    /**
     * method get information about billing of current user
     * @param userId - serial number of current user
     * @return information about billing
     * @throws DaoException
     */
    public Billing getBillingByUserId(int userId)throws DaoException{

        Billing billing = new Billing();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_BILLING_BY_USER_ID)) {
            preparedStatement.setInt(1,userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                billing.setBillingId(resultSet.getInt(DBFields.ID));
                billing.setMoneyAmount(resultSet.getBigDecimal(DBFields.MONEY_AMOUNT));
            }
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        } catch (SQLException e){
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        }
        return billing;
    }

    public Billing getBillingById(int billingId) throws DaoException{
        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_BILLING_BY_USER_ID)){

            preparedStatement.setInt(1,billingId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Billing billing = new Billing();

            if(resultSet.next()){
                billing.setBillingId(resultSet.getInt(DBFields.ID));
                billing.setMoneyAmount(resultSet.getBigDecimal(DBFields.MONEY_AMOUNT));
            }
            return billing;
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        }
    }

    public void addBilling(int userId, BigDecimal money) throws DaoException{
        try {
            Connection connection = pool.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_LAST_BILLING_ID);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_BILLING);
                int billingId = rs.getInt(DBFields.ID) + 1;

                preparedStatement.setInt(1, billingId);
                preparedStatement.setInt(2,userId);
                preparedStatement.setBigDecimal(3, money);

                preparedStatement.executeUpdate();
            }
            rs.close();
            preparedStatement.close();
            connection.close();
        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }
}
