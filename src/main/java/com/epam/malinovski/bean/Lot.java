package com.epam.malinovski.bean;

import java.math.BigDecimal;

/**
 * @author Denis
 */
public class Lot {
    private int lotId;
    private int state;
    private User user;
    private Binding binding;
    private String name;
    private BigDecimal price;
    private String description;

    public Lot(){

    }

    public Lot(int lotId, int state, User user, String name, BigDecimal price, String description){
        this.lotId = lotId;
        this.state = state;
        this.user = user;
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public Lot(int lot_id, int state, User user, Binding binding, String name, BigDecimal price,
               String description ){
        this.lotId = lot_id;
        this.state = state;
        this.user = user;
        this.binding = binding;
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Binding getBinding() {
        return binding;
    }

    public void setBinding(Binding binding) {
        this.binding = binding;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "lot_id=" + lotId +
                ", state=" + state +
                ", user=" + user +
                ", binding=" + binding +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
