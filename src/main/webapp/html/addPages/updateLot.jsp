<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 10.07.2017
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<html>
<head>
    <fmt:setBundle basename="locale"/>
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="label.update.lot"/> </title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru}">
        <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>
    <script src="${pageContext.request.contextPath}/js/addLotValidation.js"></script>

</head>
<body>
<div class="container">
 <div class="col-md-4 col-md-offset-4">
<form id="add_lot_form"  method="post" class="form-signin" action="${pageContext.request.contextPath}/Control">
    <p id="message" class="has-error">${message}</p>

    <input type="hidden" name="lotId" id="lotId" value="<c:out value="${lot.lotId}"/>">

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="lot.name"/></label></h2>
    <div class="form-group">
        <input class="form-control" required minlength="4" name="lot_name" id="lot_name"  value="<c:out value="${lot.name}" />">
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="lot.description"/></label></h2>
    <div class="form-group">
        <textarea maxlength ="255" required minlength="4" cols="20" wrap="soft"  class="form-control" type="text" name="description" placeholder="<fmt:message key="lot.description"/> " id="description"><c:out value="${lot.description}"/></textarea>
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="lot.price"/></label></h2>
    <div class="form-group">
        <input class="form-control" required min="1" name="price" id="price" value="<c:out value="${lot.price}" />">
    </div>
    <input type="hidden" name="status" id="status" value="<c:out value="${lot.state}" />">

    <input class="btn btn-success" type="submit" value="<fmt:message key="label.update.lot"/> ">
    <input type="hidden" name="cmd" value="updateLotCmd">

</form>
    </div>

    <div class="col-md-4 col-md-offset-4">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelCmd">
        </form>
    </div>
</div>
</body>
</html>
