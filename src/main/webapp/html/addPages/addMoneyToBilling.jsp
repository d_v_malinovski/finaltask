<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 31.07.2017
  Time: 23:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setBundle basename="locale"/>
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <title><fmt:message key="label.add.money"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru }">
        <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>

    <script src="${pageContext.request.contextPath}/js/moneyFormValidation.js"></script>
</head>
<body>
<div class="container">
    <div class="col-md-4 col-md-offset-4">
    <p id="message" class="has-error text-center">${message}</p>
<form id="add_money_form"  class="form-signin" method="post" action="${pageContext.request.contextPath}/Control">
    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="money.amount"/></label></h2>
    <div class="form-group">
    <input class="form-control" required min="1" name="money" placeholder="<fmt:message key="money.amount"/>">
    </div>

    <input class="btn btn-success" type="submit" value="<fmt:message key="label.add.money"/> ">
    <input type="hidden" name="cmd" value="replenishBillingCmd">
</form>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelCmd">
        </form>
    </div>
</div>
</body>
</html>
