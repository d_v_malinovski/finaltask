package com.epam.malinovski.service.impl;

import com.epam.malinovski.bean.Billing;
import com.epam.malinovski.dao.BillingDao;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.DaoFactory;
import com.epam.malinovski.service.BillingService;
import com.epam.malinovski.service.ServiceException;

import java.math.BigDecimal;

/**
 * @author Denis
 */
public class BillingServiceImpl implements BillingService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    private BillingServiceImpl(){

    }

    private static final class SingletonHolder{
        private static final BillingService billingService = new BillingServiceImpl();
    }

    public static BillingService getBillingService(){
        return SingletonHolder.billingService;
    }

    @Override
    public void createMoneyExchange(int newOwnerId, int oldOwnerBillingId, BigDecimal amount) throws ServiceException {
        try {
            BillingDao billingDao = daoFactory.getBillingDao();
            billingDao.createMoneyExchange(newOwnerId, oldOwnerBillingId, amount);
        }catch (DaoException ex){
            throw new ServiceException("Billing Service", ex);
        }
    }

    @Override
    public void setMoneyAmountToBillingById(int billingById, BigDecimal moneyAmount) throws ServiceException {
        try {
            BillingDao billingDao = daoFactory.getBillingDao();
            billingDao.setMoneyAmountToBillingById(billingById,moneyAmount);
        }catch (DaoException ex){
            throw new ServiceException("Billing Service", ex);
        }
    }

    @Override
    public Billing getBillingByUserId(int userId) throws ServiceException {
        Billing billing;
        try {
            BillingDao billingDao = daoFactory.getBillingDao();
            billing = billingDao.getBillingByUserId(userId);
            return billing;
        }catch (DaoException ex){
            throw new ServiceException("Billing Service", ex);
        }
    }

    @Override
    public Billing getBillingById(int billingId) throws ServiceException {
        Billing billing;
        try {
            BillingDao billingDao = daoFactory.getBillingDao();
            billing = billingDao.getBillingById(billingId);
            return billing;
        }catch (DaoException ex){
            throw new ServiceException("Billing Service", ex);
        }
    }

    @Override
    public void addBilling(int userId, BigDecimal money) throws ServiceException {
        try {
            BillingDao billingDao = daoFactory.getBillingDao();
            billingDao.addBilling(userId, money);
        }catch (DaoException ex){
            throw new ServiceException("Billing Service", ex);
        }
    }
}
