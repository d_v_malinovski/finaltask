package com.epam.malinovski.constant;

/**
 * @author Denis
 */

public class ButtonsCommands{
    public static final String COMMAND = "cmd";

    //login page
    public static final String LOGIN = "loginCmd";
    public static final String LOGOUT = "logOutCmd";

    //main page
    public static final String GET_ALL_USERS = "getUserList";
    public static final String CHANGE_LANG = "changeLangCmd";

    //add user page
    public static final String ADD_USER = "addUserCmd";
    public static final String REDIRECT_TO_ADD_USER = "redirectToAddUserCmd";

    //update user page
    public static final String UPDATE_USER = "updateUserCmd";
    public static final String REDIRECT_TO_UPDATE_USER = "redirectToUpdateUserCmd";
    public static final String DELETE_USER = "deleteUserCmd";

    public static final String GET_ALL_BILLINGS = "getAllBillingCmd";

    public static final String REDIRECT_TO_REPLENISH_BILLING = "redirectReplenishCmd";
    public static final String ADD_MONEY_AMOUNT_TO_BILLING = "replenishBillingCmd";

    public static final String REDIRECT_TO_USERS_MANAGEMENT = "redirectToUsersManagementCmd";
    public static final String REDIRECT_TO_LOT_MANAGEMENT = "redirectToLotManagementCmd";
    public static final String GET_USERS_LOTS = "getUsersLotsCmd";
    public static final String ADD_LOT = "addLotCmd";
    public static final String REDIRECT_TO_ADD_LOT = "redirectToAddLotCmd";
    public static final String REDIRECT_TO_UPDATE_LOT = "redirectToUpdateLotCmd";
    public static final String UPDATE_LOT = "updateLotCmd";
    public static final String SET_LOT_FOR_SALE = "setLotForSaleCmd";
    public static final String REDIRECT_TO_BINDING_MANAGEMENT = "redirectToBindingManagementCmd";
    public static final String GET_LOT_FOR_SALE = "GetOffersLotCmd";
    public static final String BLOCKING_LOT = "blockingLotCmd";
    public static final String REDIRECT_TO_ADD_BINDING = "redirectToAddBindingCmd";
    public static final String CREATE_BINDING = "addBindingCmd";
    public static final String GET_ALL_ACTIVE_BINDINGS = "getAllBindingCmd";
    public static final String FILMED_BINDING = "filmedBindingCmd";
    public static final String REDIRECT_TO_BINDING_USER_MENU = "redirectToBindingMenuCmd";
    public static final String GET_BINDING_FOR_USER = "getBindingForUserCmd";
    public static final String REDIRECT_TO_BIND_DETAIL = "redirectToBindDetailCmd";

    public static final String ADD_BET_FOR_INTERNET_BINDING = "addBetForInternetBindingCmd";
    public static final String ADD_BET_FOR_ENGLISH_BINDING = "addBetForEnglishBindingCmd";
    public static final String ADD_BET_FOR_BLITZ_BINDING = "addBetForBlitzBindingCmd";

    public static final String ADD_INTERNET_BINDING = "addInternetBindingCmd";
    public static final String ADD_ENGLISH_BINDING = "addEnglishBindingCmd";
    public static final String ADD_BLITZ_BINDING = "addBlitzBindingCmd";

    public static final String REDIRECT_TO_PAYMENT_LIST = "redirectToPaymentCmd";
    public static final String GET_ALL_PAYMENTS = "getPaymentListCmd";
    public static final String SENT_PAYMENT =  "payLotCmd";

    public static final String CANCEL_PAYMENT = "cancelPaymentCmd";
    public static final String CANCEL_ADDING = "cancelCmd";
    public static final String REDIRECT_TO_REGISTRATION_PAGE = "redirectToRegistrationCmd";
    public static final String REGISTRATION_USER = "registrationCmd";
    public static final String CANCEL_REGISTRATION = "cancelRegistrationCmd";


}
