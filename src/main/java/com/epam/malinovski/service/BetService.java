package com.epam.malinovski.service;

import com.epam.malinovski.bean.Bet;

import java.util.List;

/**
 * Created by Denis on 05.08.2017.
 */
public interface BetService extends GenericService<Bet> {
    @Override
    default List<Bet> showAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }
    int getLastBetFromCurrentBinding(int bindingId) throws ServiceException ;
}
