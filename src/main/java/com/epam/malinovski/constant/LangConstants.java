package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class LangConstants {
    public static final String BUNDLE = "bundle";
    public static final String RU = "ru_RU";
    public static final String EN = "en_US";
    public static final String LANG_BUTTON = "langBtn";
    public static final String PATH = "path";
    public static final String ATTRIBUTE_BUTTON_RU = "ru";
    public static final String ATTRIBUTE_BUTTON_EN= "en";
}
