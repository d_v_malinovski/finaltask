package com.epam.malinovski.connectionpool.impl;

import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.connectionpool.Pool;
import com.epam.malinovski.constant.DBConstant;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ResourcesPathes;
import com.epam.malinovski.util.PropertiesManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class creates connections and manage them
 * @author Denis
 */

public class ConnectionPool implements Pool {
    private static Logger logger = Logger.getLogger(ConnectionPool.class);
    private static final ConnectionPool connectionPool = new ConnectionPool();

    private BlockingQueue<Connection> freeConnection;
    private BlockingQueue<Connection> busyConnection;
    private int poolSize;

    PropertiesManager dbManager;
    private static final int DEFAULT_SIZE = 10;

    private static ReentrantLock lock = new ReentrantLock();

    public static ConnectionPool getInstance(){
        return connectionPool;
    }

    private ConnectionPool() {
        dbManager = new PropertiesManager(ResourcesPathes.DB_RESOURCE_LOCATION);
        try {
            poolSize = Integer.parseInt(dbManager.getValue(DBConstant.CONNECTION_NUMBER));
        }catch (NumberFormatException ex){
            poolSize = DEFAULT_SIZE;
        }

    }

    @Override
    public void init() throws ConnectionLevelException {
        try {
            Class.forName(dbManager.getValue(DBConstant.DRIVER));
            freeConnection = new ArrayBlockingQueue<Connection>(poolSize);
            busyConnection = new ArrayBlockingQueue<Connection>(poolSize);
                for(int i = 0; i < poolSize; i++){
                    Connection connection = DriverManager.getConnection(dbManager.getValue(DBConstant.URL),
                                                                        dbManager.getValue(DBConstant.USER),
                                                                        dbManager.getValue(DBConstant.PASSWORD));
                    ConnectionWrap connectionWrap = new ConnectionWrap(connection);
                    freeConnection.add(connectionWrap);
                }
        }catch (SQLException ex){
            throw new ConnectionLevelException("SQL EXCEPTION", ex);
        }catch (ClassNotFoundException ex){
            throw new ConnectionLevelException("CLASS NOT FOUND EXCEPTION", ex);
        }
    }

    @Override
    public void dispose() throws ConnectionLevelException {
        closeAll();
    }

    /**
     * Get free connection pool
     * @return Connection
     * @throws ConnectionLevelException
     */
    public Connection getConnection() throws ConnectionLevelException{
        Connection connection = null;

        try {
            connection = freeConnection.take();
            busyConnection.add(connection);
        } catch (InterruptedException e) {
            throw new ConnectionLevelException(ExceptionMessage.INTERRUPTED + e.getMessage(), e);
        }
        return connection;
    }

    @Override
    public void putConnection() {
        freeConnection.add(busyConnection.poll());
    }

    /**
     * Closes all connections
     */
    private void closeAll() {
      closeConnectionQueue(busyConnection);
      closeConnectionQueue(freeConnection);
    }

    private void closeConnectionQueue( BlockingQueue<Connection> connectionBlockingQueue){
        Connection connection;
        while ((connection = connectionBlockingQueue.poll())!= null){
            try {
                    if (!connection.getAutoCommit()){
                        connection.rollback();
                    }
                ((ConnectionWrap)connection).reallyClose();
            }catch (SQLException ex){
                logger.error(ExceptionMessage.CP_CLOSE_FAIL,ex);
            }
        }
    }

    private class ConnectionWrap implements Connection{
        private Connection connection;

        public ConnectionWrap(Connection connection) throws SQLException{
            this.connection = connection;
            this.connection.setAutoCommit(true);
        }

        public void reallyClose() throws SQLException{
            this.connection.close();
        }

        @Override
        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        @Override
        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);
        }

        @Override
        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        @Override
        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        @Override
        public void setAutoCommit(boolean autoCommit) throws SQLException {
                connection.setAutoCommit(autoCommit);
        }

        @Override
        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        @Override
        public void commit() throws SQLException {
                connection.commit();
        }

        @Override
        public void rollback() throws SQLException {
                connection.rollback();
        }

        @Override
        public void close() throws SQLException {
              if(connection.isClosed()){
                  throw new SQLException("Closed connection is never be closed");
              }
              if(connection.isReadOnly()){
                  connection.setReadOnly(false);
              }
              if(!connection.getAutoCommit()){
                  connection.setAutoCommit(true);
              }
              putConnection();
        }

        @Override
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        @Override
        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        @Override
        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        @Override
        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        @Override
        public void setCatalog(String catalog) throws SQLException {
                connection.setCatalog(catalog);
        }

        @Override
        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        @Override
        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        @Override
        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        @Override
        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        @Override
        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
                connection.setTypeMap(map);
        }

        @Override
        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        @Override
        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        @Override
        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        @Override
        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback(savepoint);
        }

        @Override
        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency,resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql,columnIndexes);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        @Override
        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        @Override
        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        @Override
        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        @Override
        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        @Override
        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        @Override
        public void setClientInfo(String name, String value) throws SQLClientInfoException {
                connection.setClientInfo(name, value);
        }

        @Override
        public void setClientInfo(Properties properties) throws SQLClientInfoException {
                connection.setClientInfo(properties);
        }

        @Override
        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        @Override
        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        @Override
        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName, elements);
        }

        @Override
        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName,attributes);
        }

        @Override
        public void setSchema(String schema) throws SQLException {
                connection.setSchema(schema);
        }

        @Override
        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        @Override
        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        @Override
        public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
                connection.setNetworkTimeout(executor,milliseconds);
        }

        @Override
        public int getNetworkTimeout() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }
    }

}
