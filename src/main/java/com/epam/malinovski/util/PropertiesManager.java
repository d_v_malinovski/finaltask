package com.epam.malinovski.util;

import java.util.ResourceBundle;

/**
 * @author Denis
 * This class load bundle and message from key
 */
public class PropertiesManager {
    private static ResourceBundle bundle;

    public PropertiesManager (String filePath) {
        bundle = ResourceBundle.getBundle(filePath);
    }
    /**
     * This method return property by key
     * @param key a name of property
     * @return value of property
     */
    public String getValue(String key) {
        return bundle.getString(key);
    }
}
