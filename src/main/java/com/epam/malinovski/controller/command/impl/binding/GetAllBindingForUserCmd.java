package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Method get all auction for user role
 */
public class GetAllBindingForUserCmd implements Command {
    private static Logger logger = Logger.getLogger(GetAllBindingForUserCmd.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    @Override
    public String execute(HttpServletRequest request) {
        try {
            int user_id = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);
            request.setAttribute(Attributes.BINDINGS, bindingService.getActiveBindings(user_id));
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
        }
        return PagePathes.PATH_TO_BINDING_USER_MENU;
    }
}
