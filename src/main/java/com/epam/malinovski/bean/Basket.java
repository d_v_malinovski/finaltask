package com.epam.malinovski.bean;

/**
 * @author Denis
 */
public class Basket {
    private int basketId;
    private int ownerId;
    private Lot product;
    private int billingId;

    public Basket() {
    }

    public Basket(int basketId, int ownerId, Lot product, int billingId) {
        this.basketId = basketId;
        this.ownerId = ownerId;
        this.product = product;
        this.billingId = billingId;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Lot getProduct() {
        return product;
    }

    public void setProduct(Lot product) {
        this.product = product;
    }

    public int getBillingId() {
        return billingId;
    }

    public void setBillingId(int billingId) {
        this.billingId = billingId;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "basketId=" + basketId +
                ", ownerId=" + ownerId +
                ", product=" + product +
                ", billingId=" + billingId +
                '}';
    }
}
