package com.epam.malinovski.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Denis
 * Interface with some set of methods, which must correspond to dao pattern
 * to exhchange data between app and db
 */

public interface GenericDao<T> {
    void insert(T data) throws DaoException;
    void update(T data) throws DaoException;
    void delete(T data) throws DaoException;
    List<T> select(T data) throws DaoException, SQLException;
}
