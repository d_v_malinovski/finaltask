<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 09.08.2017
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <fmt:setBundle basename="locale"/>
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <title><fmt:message key="label.registration"/> </title>

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru }">
        <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>
    <script src="${pageContext.request.contextPath}/js/createUserValidate.js"></script>
</head>
<body>
<div class="container">
    <div class="col-md-4 col-md-offset-4">
        <form id="add_user_form" class="form-signin" method="post" action="${pageContext.request.contextPath}/Control">
            <p id="message" class="has-error">${message}</p>
            <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="label.login"/></label></h2>
            <div class="form-group">
                <input class="form-control" type="text" required minlength="4" maxlength="50" name="login" placeholder="<fmt:message key="label.login"/>" id="login">
            </div>

            <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="label.password"/></label></h2>
            <div class="form-group">
                <input class="form-control" required minlength="4" maxlength="50" type="password" name="password" placeholder="<fmt:message key="label.password"/>" id="password">
            </div>
            <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="label.language"/></label></h2>
            <br>
            <br>
            <div class="radio col-md-offset-2">
                <input checked  type="radio" name="preform_lang" value="en"><fmt:message key="label.english"/>
            </div>
            <div class="radio col-md-offset-2">
                <input type="radio" name="preform_lang" value="ru"><fmt:message key="label.russian"/>
            </div>

            <input type="submit" class="btn btn-success" value="<fmt:message key="label.add.user"/> ">

            <input type="hidden" name="cmd" value="registrationCmd">
        </form>
    </div>

    <div class="col-md-4 col-md-offset-4">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelRegistrationCmd">
        </form>
    </div>
</div>
</body>
</html>
