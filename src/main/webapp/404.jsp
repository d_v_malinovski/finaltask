<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 03.07.2017
  Time: 13:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Error Page</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">
    <div class="row pad-top text-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1><strong> 404 ! </strong> ERROR</h1>
            <h3>Resource Not Found</h3>
        </div>
    </div>
</div>
</body>
</html>
