/* 
 * All content copyright Terracotta, Inc., unless otherwise indicated. All rights reserved. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
 
package com.epam.malinovski.util;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.service.BetService;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BetServiceImpl;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;
import org.quartz.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * This class create job for close auction were time of end is over
 * </p>
 * 
 * @author Denis
 */
@DisallowConcurrentExecution
public class BindingStateChecker implements Job {
    private static Logger logger = Logger.getLogger(BindingStateChecker.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    private static final LotService lotService = LotServiceImpl.getLotService();
    private static final BetService betService = BetServiceImpl.getBetService();

    public BindingStateChecker() {
        super();
    }

    public void execute(JobExecutionContext context)
        throws JobExecutionException {

            JobKey jobKey = context.getJobDetail().getKey();
            logger.info(jobKey + " " + new Date());

        List<Lot> lots;

        try {

            lots = bindingService.getBindingsWithOpenStateAndType(DBFields.BINDING_OPEN);

            if (lots.isEmpty()) {
                logger.info(jobKey + " " + new Date());
                return;
            }

            for (Lot lot : lots) {

                int binding_id = lot.getBinding().getId();
                int lot_id = lot.getLotId();

                int user_id = identifyWinner(lot);

                if (user_id == 0) {
                    lotService.changeLotState(lot_id, DBFields.LOT_ABORTED_SALE);
                    bindingService.deleteBindingByID(binding_id);
                } else {
                    bindingService.closeBinding(binding_id, user_id, lot_id);
                }
            }

        }catch (ServiceException e) {
            logger.error(e.getMessage(), e);
        }
        logger.info(jobKey + " " + new Date());
    }

    private int identifyWinner(Lot lot){
        int userId = 0;

        try {
            userId = betService.getLastBetFromCurrentBinding(lot.getBinding().getId());
        } catch (ServiceException e) {
           logger.error("Bet Service ", e);
        }

        return userId;
    }

}
