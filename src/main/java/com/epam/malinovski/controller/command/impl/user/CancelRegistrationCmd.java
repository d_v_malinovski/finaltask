package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Denis
 * Cancel command to registration user
 */
public class CancelRegistrationCmd implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session != null) {
            session.removeAttribute(AccessConstants.ROLE);
        }
        return PagePathes.PATH_TO_LOGIN_PAGE;
    }
}
