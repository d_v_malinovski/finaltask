package com.epam.malinovski.service;

import com.epam.malinovski.bean.Basket;

import java.util.List;

/**
 * Created by Denis on 05.08.2017.
 */
public interface BasketService extends GenericService<Basket> {
    @Override
    default void save(Basket data) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    default List<Basket> showAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }
    void deletePaymentById(int basketId) throws ServiceException;
    List<Basket> getAllProductToPayment(int userId) throws ServiceException;
}
