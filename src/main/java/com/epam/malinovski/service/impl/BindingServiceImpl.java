package com.epam.malinovski.service.impl;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.dao.BindingDao;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.DaoFactory;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.ServiceException;

import java.util.List;

/**
 * @author Denis
 */
public class BindingServiceImpl implements BindingService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    private BindingServiceImpl(){

    }
    private static class SingletonHolder{
        private static final BindingService bindingService = new BindingServiceImpl();
    }
    public static BindingService getBindingService(){
        return SingletonHolder.bindingService;
    }

    @Override
    public List<Lot> getActiveBindings(int userId) throws ServiceException {
        List<Lot> lots;
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            lots = bindingDao.getActiveBindings(userId);
            return lots;
        }catch (DaoException ex){
            throw new ServiceException("Binding Service", ex);
        }
    }

    @Override
    public void deleteBindingByID(int id) throws ServiceException {
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            bindingDao.deleteBindingByID(id);
        }catch (DaoException ex){
            throw new ServiceException("Binding Service", ex);
        }
    }

    @Override
    public List<Lot> getBindingsWithOpenStateAndType(int statusId) throws ServiceException {
        List<Lot> lots;
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            lots = bindingDao.getBindingsWithOpenStateAndType(statusId);
            return lots;
        }catch (DaoException ex){
            throw new ServiceException("Binding Service", ex);
        }
    }

    @Override
    public void closeBinding(int bindingId, int userId, int lotId) throws ServiceException {
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            bindingDao.closeBinding(bindingId,userId,lotId);
        }catch (DaoException ex){
            throw new ServiceException("Binding Service", ex);
        }
    }

    @Override
    public void save(int lotId, Binding binding) throws ServiceException {
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            if(binding.getType() == DBFields.BINDING_ENGLISH){
                bindingDao.addEnglishBinding(lotId, binding);
            }
            if(binding.getType() == DBFields.BINDING_BLITZ){
                bindingDao.addBlitzBinding(lotId, binding);
            }
            if(binding.getType() == DBFields.BINDING_INTERNET){
                bindingDao.addInternetBinding(lotId, binding);
            }
        }catch (DaoException ex){
            throw new ServiceException("Binding Service",ex);
        }
    }

    @Override
    public List<Lot> getAllBindings() throws ServiceException {
        List<Lot> lots;
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            lots = bindingDao.getActiveBindings();
            return lots;
        }catch (DaoException ex){
            throw new ServiceException("Binding Service, ex");
        }
    }

    @Override
    public Binding getBindingById(int id) throws ServiceException {
        Binding binding;
        try {
            BindingDao bindingDao = daoFactory.getBindingDao();
            binding = bindingDao.getBindingById(id);
            return binding;
        }catch (DaoException ex){
            throw new ServiceException("Binding Service", ex);
        }
    }
}
