package com.epam.malinovski.controller.command.impl.user;


import com.epam.malinovski.bean.User;
import com.epam.malinovski.constant.*;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import com.epam.malinovski.util.Messager;
import com.epam.malinovski.util.PasswordManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

/**
 * @author Denis
 * This metod need to log in in application
 */
public class LoginCmd implements Command {

    private static Logger logger = Logger.getLogger(LoginCmd.class);
    private static int USER_ROLE_ID = 1;
    private static int ADMIN_ROLE_ID = 2;
    private static final UserService userService = UserServiceImpl.getUserService();


    public String execute(HttpServletRequest request) {
        PasswordManager passwordManager = PasswordManager.getInstance();
        String login = request.getParameter(Attributes.LOGIN);
        String password = request.getParameter(Attributes.PASSWORD);
        HttpSession session = request.getSession(true);
        try {
            User account = userService.getUserByLogin(login);
            if (account == null) {
                session.setAttribute(AccessConstants.ROLE, AccessConstants.GUEST_ROLE);
                Messager.sendMessage(request, Messages.AUTHORIZATION_ERROR);
                return PagePathes.PATH_TO_LOGIN_PAGE;
            }
            if (passwordManager.comparePasswords(password, account.getPassword())) {
                    if (account.getRoleId() == ADMIN_ROLE_ID) {
                        session.setAttribute(AccessConstants.ROLE, AccessConstants.ADMIN_ROLE);
                        session.setAttribute(Attributes.CURRENT_USER_ID,account.getId());
                        Config.set(session, Config.FMT_LOCALE, account.getLanguage());
                        session.setAttribute(LangConstants.BUNDLE, account.getLanguage());
                       return PagePathes.PATH_TO_ADMIN_PAGE;
                    }
                    if(account.getRoleId() == USER_ROLE_ID) {
                        session.setAttribute(AccessConstants.ROLE, AccessConstants.USER_ROLE);
                        session.setAttribute(Attributes.CURRENT_USER_ID,account.getId());
                        Config.set(session, Config.FMT_LOCALE, account.getLanguage());
                        session.setAttribute(LangConstants.BUNDLE, account.getLanguage());
                        return PagePathes.PATH_TO_USER_PAGE;
                    }
            } else {
                session.setAttribute(AccessConstants.ROLE, AccessConstants.GUEST_ROLE);
                Messager.sendMessage(request, Messages.AUTHORIZATION_ERROR);
                return PagePathes.PATH_TO_LOGIN_PAGE;
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
        }
        return PagePathes.PATH_TO_LOGIN_PAGE;
    }


}
