package com.epam.malinovski.dao;

/**
 * @author Denis
 */
public class DaoException extends Exception {
    public DaoException(String message){
        super(message);
    }

    public DaoException(String message, Exception ex){
        super(message, ex);
    }
}
