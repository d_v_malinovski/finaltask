package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method need to bloking lot
 */
public class BlockingLotCmd implements Command {
    private static Logger logger = Logger.getLogger(BlockingLotCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    @Override
    public String execute(HttpServletRequest request) {

       int id = Integer.parseInt(request.getParameter(Attributes.LOT_ID));
       int state = DBFields.LOT_BLOCKING;

        try {
            lotService.changeLotState(id, state);
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
           return PagePathes.PATH_TO_BINDING_PAGE;
        }
        return PagePathes.PATH_TO_BINDING_PAGE;
    }
}
