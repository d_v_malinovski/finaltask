package com.epam.malinovski.filter;


import com.epam.malinovski.constant.PagePathes;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Denis
 * This filter class is designed to monitor unauthorized access to web pages.
 */
public class AuthorizationFilter implements Filter {
    private static Logger logger = Logger.getLogger(AuthorizationFilter.class);


    public void init(FilterConfig filterConfig) throws ServletException {
    }


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        httpRequest.getRequestDispatcher(PagePathes.PATH_TO_ERROR_PAGE).forward(httpRequest, httpResponse);
    }


    public void destroy() {}
}
