package com.epam.malinovski.service;

import com.epam.malinovski.bean.Billing;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Denis on 05.08.2017.
 */
public interface BillingService extends GenericService<Billing> {
    @Override
    default void save(Billing data) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    default List<Billing> showAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }

    void createMoneyExchange(int newOwnerId, int oldOwnerBillingId, BigDecimal amount) throws ServiceException;
    void setMoneyAmountToBillingById(int billingById, BigDecimal moneyAmount) throws ServiceException;
    Billing getBillingByUserId(int userId)throws ServiceException;
    Billing getBillingById(int billingId) throws ServiceException;
    void addBilling(int userId, BigDecimal money) throws ServiceException;
}
