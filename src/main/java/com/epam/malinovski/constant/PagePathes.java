package com.epam.malinovski.constant;

/**
 * @author Denis
 */
public class PagePathes {
    public static final String PATH_TO_USER_PAGE = "html/mainPages/mainUserPage.jsp";
    public static final String PATH_TO_ADMIN_PAGE = "html/mainPages/admin.jsp";
    public static final String PATH_TO_LOGIN_PAGE = "login.jsp";

    public static final String PATH_TO_ERROR_PAGE = "404.jsp";
    public static final String PATH_TO_ADD_USER_PAGE = "html/addPages/addUser.jsp";
    public static final String PATH_TO_UPDATE_USER_PAGE = "html/addPages/updateUser.jsp";
    public static final String PATH_TO_REPLENISH_BILLING = "html/addPages/addMoneyToBilling.jsp";
    public static final String PATH_TO_ADD_BILLING = "html/addPages/addBilling.jsp";

    public static final String PATH_TO_BILLING_PAGE = "html/showPages/billingPage.jsp";
    public static final String PATH_TO_MANAGEMENT_USERS = "html/showPages/managementClient.jsp";
    public static final String PATH_TO_BINDING_PAGE = "html/showPages/bindingPage.jsp";
    public static final String PATH_TO_LOT_PAGE = "html/showPages/lotPage.jsp";

    public static final String PATH_TO_ADD_LOT = "html/addPages/addLot.jsp";
    public static final String PATH_TO_UPDATE_LOT = "html/addPages/updateLot.jsp";
    public static final String PATH_TO_BINDING_USER_MENU = "html/showPages/bindingPageForUser.jsp";

    public static final String PATH_TO_ENGLISH_BINDING_DETAIL = "html/showPages/englishBinding.jsp";
    public static final String PATH_TO_BLITZ_BINDING_DETAIL = "html/showPages/blitzBinding.jsp";
    public static final String PATH_TO_INTERNET_BINDING_DETAIL = "html/showPages/internetBinding.jsp";

    public static final String PATH_TO_ADD_BLITZ_BINDING = "html/addPages/createBlitzBinding.jsp";
    public static final String PATH_TO_ADD_ENGLISH_BINDING = "html/addPages/createEnglishBinding.jsp";
    public static final String PATH_TO_ADD_INTERNET_BINDING = "html/addPages/createInternetBinding.jsp";
    public static final String PATH_TO_PAYMENT_LIST = "html/showPages/paymentListPage.jsp";
    public static final String PATH_TO_REGISTRATION_PAGE = "html/addPages/registrationPage.jsp";
}
