package com.epam.malinovski.controller.command.impl.payment;

import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BillingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BillingServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * @author Denis
 * This method need to add money amount to billing
 */
public class ReplenishBillingCmd implements Command {
    private Logger logger = Logger.getLogger(ReplenishBillingCmd.class);
    private static final BillingService billingService = BillingServiceImpl.getBillingService();
    @Override
    public String execute(HttpServletRequest request) {
     try {

        int id = (int) request.getSession().getAttribute(Attributes.BILLING_ID);

        BigDecimal money = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.MONEY_AMOUNT)));

         if(money.doubleValue() <= 0) {
             Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
             return PagePathes.PATH_TO_REPLENISH_BILLING;
         }

            billingService.setMoneyAmountToBillingById(id, money);

        } catch (ServiceException | IllegalArgumentException e) {
           logger.error(e.getMessage(),e);
           Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
           return PagePathes.PATH_TO_REPLENISH_BILLING;
        }
        request.getSession().removeAttribute(Attributes.BILLING_ID);
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_USER_PAGE);

        return PagePathes.PATH_TO_USER_PAGE;
    }
}
