package com.epam.malinovski.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis
 */
public class Binding {
    private int id;
    private int state;
    private Date start;
    private Date end;
    private int type;
    private int numberOfParticipant;
    private BigDecimal bindingStep;
    private BigDecimal blitzPrice;
    private BigDecimal endPrice;

    public Binding() {
    }

    public Binding(int id, int state, Date start, Date end, int type) {
        this.id = id;
        this.state = state;
        this.start = start;
        this.end = end;
        this.type = type;
    }

    public Binding(int id, int state, Date start, Date end, int type, int numberOfParticipant,
                   BigDecimal bindingStep, BigDecimal blitzPrice) {
        this.id = id;
        this.state = state;
        this.start = start;
        this.end = end;
        this.type = type;
        this.numberOfParticipant = numberOfParticipant;
        this.bindingStep = bindingStep;
        this.blitzPrice = blitzPrice;
    }

    public Binding(int id, int state, Date start, Date end, int type, BigDecimal endPrice) {
        this.id = id;
        this.state = state;
        this.start = start;
        this.end = end;
        this.type = type;
        this.endPrice = endPrice;
    }

    public int getNumberOfParticipant() {
        return numberOfParticipant;
    }

    public void setNumberOfParticipant(int numberOfParticipant) {
        this.numberOfParticipant = numberOfParticipant;
    }

    public BigDecimal getBindingStep() {
        return bindingStep;
    }

    public void setBindingStep(BigDecimal bindingStep) {
        this.bindingStep = bindingStep;
    }

    public BigDecimal getBlitzPrice() {
        return blitzPrice;
    }

    public void setBlitzPrice(BigDecimal blitzPrice) {
        this.blitzPrice = blitzPrice;
    }

    public BigDecimal getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(BigDecimal endPrice) {
        this.endPrice = endPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Binding{" +
                "id=" + id +
                ", state=" + state +
                ", start=" + start +
                ", end=" + end +
                ", type=" + type +
                '}';
    }
}
