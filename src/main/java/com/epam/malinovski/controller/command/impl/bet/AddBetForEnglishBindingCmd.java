package com.epam.malinovski.controller.command.impl.bet;

import com.epam.malinovski.bean.Bet;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.*;
import com.epam.malinovski.service.impl.BetServiceImpl;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.service.impl.UserServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author Denis
 * Set bet for english auction
 */
public class AddBetForEnglishBindingCmd implements Command {
    private static Logger logger = Logger.getLogger(AddBetForEnglishBindingCmd.class);
    private static int NULL_VALUE = 0;

    private static final UserService userService = UserServiceImpl.getUserService();
    private static final LotService lotService = LotServiceImpl.getLotService();
    private static final BetService betService = BetServiceImpl.getBetService();
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();

    @Override
    public String execute(HttpServletRequest request) {
    try {

        int userId = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);
        int bindingId = Integer.parseInt(request.getParameter(Attributes.BINDING_ID));

        BigDecimal currentPrice = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.BET)));
        BigDecimal endPrice = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.END_PRICE)));

        Timestamp current = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());

        Timestamp start = Timestamp.valueOf(request.getParameter(Attributes.START));
        Timestamp end = Timestamp.valueOf(request.getParameter(Attributes.END));

        int lotId = Integer.parseInt(request.getParameter(Attributes.VARIANT_LOT_ID));

        if(currentPrice.doubleValue() <= lotService.getLotById(lotId).getPrice().doubleValue()||
                currentPrice.doubleValue() <= NULL_VALUE){
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
            return PagePathes.PATH_TO_ENGLISH_BINDING_DETAIL;
        }

        if(start.after(current) || end.before(current)){
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
            return PagePathes.PATH_TO_INTERNET_BINDING_DETAIL;
        }

        Bet bet = new Bet();

        bet.setUserId(userId);
        bet.setBindingId(bindingId);
        bet.setBetAmount(currentPrice);
        bet.setCurrentTime(current);


            if(userService.checkMemberShip(userId, bindingId)){
                    betService.save(bet);
                    lotService.updatePriceLotById(lotId, currentPrice);

                int result = bet.getBetAmount().compareTo(endPrice);

                if(result == 0 || result == 1 ) {
                    bindingService.closeBinding(bindingId, userId, lotId);
                }

            }else {
                userService.takePartInBinding(userId, bindingId);
                    betService.save(bet);
                    lotService.updatePriceLotById(lotId, currentPrice);
                int result = bet.getBetAmount().compareTo(endPrice);

                if(result == 0 || result == 1 ) {
                    //завершаем торги досрочно
                    bindingService.closeBinding(bindingId, userId,lotId);
                }

            }

        } catch (ServiceException | IllegalArgumentException e) {
            logger.error(e.getMessage(), e);
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
            return PagePathes.PATH_TO_ENGLISH_BINDING_DETAIL;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_BINDING_USER_MENU);

        return PagePathes.PATH_TO_BINDING_USER_MENU;
    }
}
