package com.epam.malinovski.controller.command.impl.bet;

import com.epam.malinovski.bean.Bet;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.*;
import com.epam.malinovski.service.impl.BetServiceImpl;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.service.impl.UserServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author Denis
 * Set new bet for Blitz auction
 */
public class AddBetForBlitzBindingCmd implements Command {
    private static Logger logger = Logger.getLogger(AddBetForBlitzBindingCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    private static final LotService lotService = LotServiceImpl.getLotService();
    private static final BetService betService = BetServiceImpl.getBetService();

    @Override
    public String execute(HttpServletRequest request) {

        int userId = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);
        int bindingId = Integer.parseInt(request.getParameter(Attributes.BINDING_ID));

        BigDecimal currentPrice = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.BET)));

        BigDecimal blitz = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.BLITZ)));
        BigDecimal step = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.STEP)));

        Timestamp current = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        int lotId = Integer.parseInt(request.getParameter(Attributes.VARIANT_LOT_ID));

        Timestamp start = Timestamp.valueOf(request.getParameter(Attributes.START));
        Timestamp end = Timestamp.valueOf(request.getParameter(Attributes.END));

        if(start.after(current) || end.before(current)){
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
            return PagePathes.PATH_TO_BLITZ_BINDING_DETAIL;
        }

        Bet bet = new Bet();

        bet.setUserId(userId);
        bet.setBindingId(bindingId);
        bet.setBetAmount(currentPrice.add(step));
        bet.setCurrentTime(current);

        try {

            if(userService.checkMemberShip(userId,bindingId)){
                betService.save(bet);
                lotService.updatePriceLotById(lotId, currentPrice.add(step));

                int result = bet.getBetAmount().compareTo(blitz);

                if(result == 0 || result == 1 ) {
                    bindingService.closeBinding(bindingId, userId, lotId);
                }

            }else if(!userService.checkMemberShip(userId, bindingId) &&
                    userService.checkFreePositionForMemberShip(bindingId)){

                userService.takePartInBinding(userId, bindingId);
                lotService.updatePriceLotById(lotId, currentPrice.add(step));
                betService.save(bet);

                int result = bet.getBetAmount().compareTo(blitz);

                if(result == 0 || result == 1 ) {
                    bindingService.closeBinding(bindingId, userId, lotId);
                }
            }else {
                Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
                return PagePathes.PATH_TO_BLITZ_BINDING_DETAIL;
            }

        } catch (ServiceException e) {
            logger.error(e.getMessage(), e);
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
            return PagePathes.PATH_TO_BLITZ_BINDING_DETAIL;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_BINDING_USER_MENU);

        return PagePathes.PATH_TO_BINDING_USER_MENU;
    }
}
