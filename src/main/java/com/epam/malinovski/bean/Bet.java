package com.epam.malinovski.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Denis
 */
public class Bet {
    private int betId;
    private int userId;
    private int bindingId;
    private Timestamp currentTime;
    private BigDecimal betAmount;

    public Bet() {
    }

    public Bet(int betId, int userId, int bindingId, Timestamp currentTime, BigDecimal betAmount) {
        this.betId = betId;
        this.userId = userId;
        this.bindingId = bindingId;
        this.currentTime = currentTime;
        this.betAmount = betAmount;
    }

    public int getBetId() {
        return betId;
    }

    public void setBetId(int betId) {
        this.betId = betId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBindingId() {
        return bindingId;
    }

    public void setBindingId(int bindingId) {
        this.bindingId = bindingId;
    }

    public Timestamp getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Timestamp currentTime) {
        this.currentTime = currentTime;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "betId=" + betId +
                ", userId=" + userId +
                ", bindingId=" + bindingId +
                ", currentTime=" + currentTime +
                ", betAmount=" + betAmount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bet bet = (Bet) o;

        if (bindingId != 0 ? !(bindingId == bet.getBindingId()) : bet.getBindingId() != 0) return false;
        if (userId != 0 ? !(userId == bet.getUserId()) : bet.getUserId() != 0) return false;
        if (currentTime != null ? !currentTime.equals(bet.getCurrentTime()): bet.getCurrentTime() != null) return false;

        return true;

    }
}
