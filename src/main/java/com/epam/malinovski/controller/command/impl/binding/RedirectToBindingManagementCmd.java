package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Method to get page with auction
 */
public class RedirectToBindingManagementCmd implements Command {
    private static Logger logger = Logger.getLogger(RedirectToBindingManagementCmd.class);
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)) {
            try {
                request.setAttribute(Attributes.BINDINGS, bindingService.getAllBindings());
            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
            }
            return PagePathes.PATH_TO_BINDING_PAGE;
        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
