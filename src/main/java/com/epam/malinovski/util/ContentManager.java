package com.epam.malinovski.util;


import com.epam.malinovski.constant.Messages;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.impl.binding.GetAllBindingCmd;
import com.epam.malinovski.controller.command.impl.binding.GetAllBindingForUserCmd;
import com.epam.malinovski.controller.command.impl.lot.GetUsersLotsCmd;
import com.epam.malinovski.controller.command.impl.user.GetAllUsersCmd;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Denis
 * This class set define witch kind of message will show on the page
 */
public class ContentManager {

    private static final Set<String> PATHS;

    static {
        PATHS = Collections.unmodifiableSet(Stream.of(
                PagePathes.PATH_TO_ADD_LOT,
                PagePathes.PATH_TO_ADD_BILLING,
                PagePathes.PATH_TO_ADD_BLITZ_BINDING,
                PagePathes.PATH_TO_ADD_ENGLISH_BINDING,
                PagePathes.PATH_TO_ADD_INTERNET_BINDING,
                PagePathes.PATH_TO_UPDATE_LOT,
                PagePathes.PATH_TO_ENGLISH_BINDING_DETAIL,
                PagePathes.PATH_TO_INTERNET_BINDING_DETAIL,
                PagePathes.PATH_TO_BILLING_PAGE)
                .collect(Collectors.toSet()));
    }

    /**
     * According to pageAlias it updates page's content by calling appropriate show commands
     * @param location path to the page
     * @param request request
     */
    public static void addContent(String location, HttpServletRequest request) {
        if(location.equals(PagePathes.PATH_TO_ADD_USER_PAGE) || location.equals(PagePathes.PATH_TO_UPDATE_USER_PAGE)||
                location.equals(PagePathes.PATH_TO_REGISTRATION_PAGE)){
            Messager.sendMessage(request, Messages.USER_IS_ALREADY_EXIST);
        }
        if(location.equals(PagePathes.PATH_TO_BLITZ_BINDING_DETAIL)){
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
        }
        if(location.equals(PagePathes.PATH_TO_LOGIN_PAGE)){
            Messager.sendMessage(request, Messages.AUTHORIZATION_ERROR);
        }
        if (PATHS.contains(location)){
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
        }
        if(location.equals(PagePathes.PATH_TO_PAYMENT_LIST)){
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
        }
        if(location.equals(PagePathes.PATH_TO_LOT_PAGE)) {
            new GetUsersLotsCmd().execute(request);
        }
        if(location.equals(PagePathes.PATH_TO_MANAGEMENT_USERS)) {
            new GetAllUsersCmd().execute(request);
        }
        if (location.equals(PagePathes.PATH_TO_BINDING_PAGE)){
            new GetAllBindingCmd().execute(request);
        }
        if (location.equals(PagePathes.PATH_TO_BINDING_USER_MENU)){
            new GetAllBindingForUserCmd().execute(request);
        }
        if(location.equals(PagePathes.PATH_TO_REPLENISH_BILLING)){
            Messager.sendMessage(request, Messages.UNCORRECT_OPERATION);
        }
    }
}
