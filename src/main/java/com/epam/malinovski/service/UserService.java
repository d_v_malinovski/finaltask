package com.epam.malinovski.service;

import com.epam.malinovski.bean.User;

import java.util.List;

/**
 * @author Denis
 */
public interface UserService extends GenericService<User> {
    @Override
    default List<User> showAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }
    void delete(int id) throws ServiceException;
    void update(User date) throws ServiceException;
    List<User> getAll(int id) throws ServiceException;
    User getUserByLogin(String login) throws ServiceException;
    User getUserById(int id) throws ServiceException;
    boolean checkMemberShip(int user_id, int binding_id) throws ServiceException;
    void takePartInBinding(int user_id, int binding_id) throws ServiceException;
    boolean checkFreePositionForMemberShip(int binding_id) throws ServiceException;
}
