package com.epam.malinovski.controller.command.impl.payment;

import com.epam.malinovski.bean.Billing;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BillingService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BillingServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Redirect to add money to billing
 */
public class RedirectReplenishCmd implements Command {
    private static Logger logger = Logger.getLogger(RedirectReplenishCmd.class);
    private static final BillingService billingService = BillingServiceImpl.getBillingService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {

            Billing billing = new Billing();
            try {
                int userId = (int) request.getSession().getAttribute(Attributes.CURRENT_USER_ID);

                billing = billingService.getBillingByUserId(userId);

            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
            }
            request.getSession().setAttribute(Attributes.BILLING_ID,billing.getBillingId());
            return PagePathes.PATH_TO_REPLENISH_BILLING;
        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
