package com.epam.malinovski.controller.command.impl.lot;

import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.PagePathes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method redirect to add lot page
 */
public class RedirectToAddLotCmd implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {
            return PagePathes.PATH_TO_ADD_LOT;
        }else return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
