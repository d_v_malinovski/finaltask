package com.epam.malinovski.connectionpool;

import java.sql.Connection;

/**
 * Provides interface for working with pool of connections
 */
public interface Pool {
    /**
     * Initialize pool with properties from bundle
     * @throws ConnectionLevelException
     */
    void init()throws ConnectionLevelException;

    /**
     * Close pool of connections
     * @throws ConnectionLevelException
     */
    void dispose() throws ConnectionLevelException;
    /**
     * Get free connection from pool     *
     * @throws ConnectionLevelException
     * @return Connection
     */
    Connection getConnection() throws ConnectionLevelException;

    /**
     * Return Connection to pool
     *
     */
    void putConnection();
}
