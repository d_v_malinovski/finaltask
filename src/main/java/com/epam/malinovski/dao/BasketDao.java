package com.epam.malinovski.dao;

import com.epam.malinovski.bean.Basket;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Denis
 */
public interface BasketDao extends GenericDao<Basket> {
    @Override
    default void insert(Basket data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default void update(Basket data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default void delete(Basket data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    default List<Basket> select(Basket data) throws DaoException, SQLException {
        throw new UnsupportedOperationException();
    }

    void deletePaymentById(int basketId) throws DaoException;
    List<Basket> getAllProductToPayment(int userId) throws DaoException;
}
