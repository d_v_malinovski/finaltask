package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.constant.*;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.BindingService;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.BindingServiceImpl;
import com.epam.malinovski.service.impl.LotServiceImpl;
import com.epam.malinovski.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author Denis
 * this command create english auction
 */
public class AddEnglishBindingCmd implements Command {
    private static Logger logger = Logger.getLogger(AddEnglishBindingCmd.class);
    private static final LotService lotService = LotServiceImpl.getLotService();
    private static final BindingService bindingService = BindingServiceImpl.getBindingService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)) {
        try {

            int lotId = Integer.parseInt(request.getParameter(Attributes.VARIANT_LOT_ID));

            String start = request.getParameter(Attributes.DATE_BEGIN) + " " + request.getParameter(Attributes.TIME_BEGIN);
            String end = request.getParameter(Attributes.DATE_END) + " " + request.getParameter(Attributes.TIME_END);

            BigDecimal end_Price = new BigDecimal(Double.parseDouble(request.getParameter(Attributes.END_PRICE)));
            BigDecimal actualPrice = lotService.getLotById(lotId).getPrice();


            Timestamp startDate = Timestamp.valueOf(start);
            Timestamp endDate = Timestamp.valueOf(end);
            Timestamp currentDate = new Timestamp(Calendar.getInstance().getTime().getTime());

            if(startDate.after(endDate) || startDate.equals(endDate) ||
               startDate.before(currentDate)|| end_Price.doubleValue() <= Attributes.NULL_VALUE ||
               actualPrice.doubleValue() >= end_Price.doubleValue()){
                Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
                return PagePathes.PATH_TO_ADD_ENGLISH_BINDING;
            }

            Binding binding = new Binding();
            binding.setStart(startDate);
            binding.setEnd(endDate);
            binding.setType(DBFields.BINDING_ENGLISH);
            binding.setState(DBFields.BINDING_OPEN);
            binding.setEndPrice(end_Price);


                bindingService.save(lotId, binding);

            } catch (ServiceException | IllegalArgumentException e) {
                logger.error(e.getMessage(),e);
                Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
                return PagePathes.PATH_TO_ADD_ENGLISH_BINDING;
            }
            request.getSession().removeAttribute(Attributes.LOT);

            request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_ADMIN_PAGE);
        }

        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
