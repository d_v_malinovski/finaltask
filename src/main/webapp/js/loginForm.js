/**
 * Created by Denis on 01.08.2017.
 */
$(function () {

    $.validator.addMethod('regexp', function(value, element, params) {
        var expression = new RegExp(params);
        return this.optional(element) || expression.test(value);
    }); 
    
    $('#login_form').validate({           
        rules:{            
            login:{
                required:true,
                minlength: 4, 
                maxlength: 50,
                regexp: /^[a-zA-Z][a-zA-Z0-9-_\.]{4,50}$/
            },

            password:{
                required:true,
                minlength: 4,
                maxlength: 50,
                regexp: /^[a-zA-Z][a-zA-Z0-9-_\.]{4,50}/
            }
        } ,        
        errorClass: 'has-error',
        onsubmit: true
    });  
    
});
