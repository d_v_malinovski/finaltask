package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.UserService;
import com.epam.malinovski.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * redirect to page for managment user
 */
public class RedirectToUserManagementCmd implements Command {
    private static Logger logger = Logger.getLogger(RedirectToUserManagementCmd.class);
    private static final UserService userService = UserServiceImpl.getUserService();
    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)) {

            try {

                int userId = (int)request.getSession().getAttribute(Attributes.CURRENT_USER_ID);

                request.setAttribute(Attributes.USERS_LIST, userService.getAll(userId));

            } catch (ServiceException e) {
                logger.error(e.getMessage(),e);
            }
            return PagePathes.PATH_TO_MANAGEMENT_USERS;
        } else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
