package com.epam.malinovski.service.impl;

import com.epam.malinovski.bean.Basket;
import com.epam.malinovski.dao.BasketDao;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.DaoFactory;
import com.epam.malinovski.service.BasketService;
import com.epam.malinovski.service.ServiceException;

import java.util.List;

/**
 * @author Denis
 */
public class BasketServiceImpl implements BasketService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    private static class SingletonHolder{
        private static final BasketService basketService = new BasketServiceImpl();
    }

    public static BasketService getBasketService(){
        return SingletonHolder.basketService;
    }

    @Override
    public void deletePaymentById(int basketId) throws ServiceException {
        try {
            BasketDao basketDao = daoFactory.getBasketDao();
            basketDao.deletePaymentById(basketId);
        }catch (DaoException ex){
            throw new ServiceException("Basket Service", ex);
        }
    }

    @Override
    public List<Basket> getAllProductToPayment(int userId) throws ServiceException {
        List<Basket> basketList;
        try {
            BasketDao basketDao = daoFactory.getBasketDao();
            basketList = basketDao.getAllProductToPayment(userId);
            return basketList;
        }catch (DaoException ex){
            throw new ServiceException("Basket Service", ex);
        }
    }
}
