package com.epam.malinovski.controller.command.impl;


import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.controller.command.impl.bet.AddBetForEnglishBindingCmd;
import com.epam.malinovski.controller.command.impl.bet.AddBetForInternetBindingCmd;
import com.epam.malinovski.controller.command.impl.binding.*;
import com.epam.malinovski.controller.command.impl.lot.*;
import com.epam.malinovski.controller.command.impl.payment.*;
import com.epam.malinovski.controller.command.impl.user.*;
import com.epam.malinovski.constant.ButtonsCommands;
import com.epam.malinovski.controller.command.impl.bet.AddBetForBlitzBindingCmd;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Denis
 * Manages command
 */
public class CommandManager {
    /**
     * Binds string parameter with command
     */
    private Map<String, Command> commands;
    private static CommandManager instance = new CommandManager();

    public static CommandManager getInstance(){
        return instance;
    }

    private CommandManager(){
        commands = new HashMap<>();
        commands.put(ButtonsCommands.LOGIN, new LoginCmd());
        commands.put(ButtonsCommands.LOGOUT, new LogOutCmd());
        commands.put(ButtonsCommands.CHANGE_LANG, new ChangeLanguageCmd());

        commands.put(ButtonsCommands.GET_ALL_USERS, new GetAllUsersCmd());

        commands.put(ButtonsCommands.ADD_USER, new AddUserCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_ADD_USER, new RedirectToAddUserCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_UPDATE_USER, new RedirectToUpdateUser());
        commands.put(ButtonsCommands.UPDATE_USER, new UpdateUserCmd());
        commands.put(ButtonsCommands.DELETE_USER, new DeleteUserCmd());

        commands.put(ButtonsCommands.REDIRECT_TO_REPLENISH_BILLING,new RedirectReplenishCmd());
        commands.put(ButtonsCommands.ADD_MONEY_AMOUNT_TO_BILLING, new ReplenishBillingCmd());


        commands.put(ButtonsCommands.REDIRECT_TO_USERS_MANAGEMENT, new RedirectToUserManagementCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_LOT_MANAGEMENT, new RedirectToLotManagementCmd());
        commands.put(ButtonsCommands.GET_USERS_LOTS, new GetUsersLotsCmd());
        commands.put(ButtonsCommands.ADD_LOT, new AddLotCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_ADD_LOT,new RedirectToAddLotCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_UPDATE_LOT, new RedirectToUpdateLot());
        commands.put(ButtonsCommands.UPDATE_LOT, new UpdateLotCmd());
        commands.put(ButtonsCommands.SET_LOT_FOR_SALE, new SetLotForSaleCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_BINDING_MANAGEMENT, new RedirectToBindingManagementCmd());
        commands.put(ButtonsCommands.GET_LOT_FOR_SALE, new GetLotsForSaleCmd());
        commands.put(ButtonsCommands.BLOCKING_LOT, new BlockingLotCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_ADD_BINDING, new RedirectToCreateBindingCmd());

        commands.put(ButtonsCommands.GET_ALL_ACTIVE_BINDINGS, new GetAllBindingCmd());
        commands.put(ButtonsCommands.FILMED_BINDING, new FilmedBindingCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_BINDING_USER_MENU,new RedirectToUserBindingMenu());
        commands.put(ButtonsCommands.GET_BINDING_FOR_USER, new GetAllBindingForUserCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_BIND_DETAIL, new RedirectToBindDetailCmd());

        commands.put(ButtonsCommands.ADD_BET_FOR_INTERNET_BINDING, new AddBetForInternetBindingCmd());
        commands.put(ButtonsCommands.ADD_BET_FOR_ENGLISH_BINDING, new AddBetForEnglishBindingCmd());
        commands.put(ButtonsCommands.ADD_BET_FOR_BLITZ_BINDING, new AddBetForBlitzBindingCmd());

        commands.put(ButtonsCommands.ADD_INTERNET_BINDING, new AddInternetBindingCmd());
        commands.put(ButtonsCommands.ADD_ENGLISH_BINDING, new AddEnglishBindingCmd());
        commands.put(ButtonsCommands.ADD_BLITZ_BINDING, new AddBlitzBindingCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_PAYMENT_LIST, new RedirectToPaymentListCmd());
        commands.put(ButtonsCommands.GET_ALL_PAYMENTS, new GetPaymentListCmd());
        commands.put(ButtonsCommands.SENT_PAYMENT, new PayLotCmd());
        commands.put(ButtonsCommands.CANCEL_PAYMENT, new AbortPayCmd());
        commands.put(ButtonsCommands.CANCEL_ADDING, new CancelCmd());
        commands.put(ButtonsCommands.REDIRECT_TO_REGISTRATION_PAGE, new RedirectToRegistrationCmd());
        commands.put(ButtonsCommands.REGISTRATION_USER, new RegistrationUserCmd());
        commands.put(ButtonsCommands.CANCEL_REGISTRATION, new CancelRegistrationCmd());
    }

    /**
     * Get command from manager by its string name
     * @param commandString command's name
     * @return Command needed
     */
    public Command getCommand(String commandString){
        return commands.get(commandString);
    }
}
