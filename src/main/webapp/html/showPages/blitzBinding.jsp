<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 21.07.2017
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session" />
    <title><fmt:message key="blitz.binding"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
</head>
<body>
<div class="container-fluid">
    <table class="table">
        <caption class=" caption text-center">

        </caption>

        <tr class="text-center">
            <th><fmt:message key="binding.start"/> </th>
            <td><c:out value = "${lot.binding.start}"/></td>
        </tr>

        <tr class="text-center">
            <th class="col-xs-3"><fmt:message key="binding.end"/></th>
            <td><c:out  value = "${lot.binding.end}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="binding.type"/> </th>
            <td><fmt:message key="blitz.binding"/> </td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="lot.name"/> </th>
            <td><c:out value="${lot.name}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="lot.price"/> </th>
            <td><c:out value="${lot.price}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="lot.description"/> </th>
            <td><textarea maxlength ="255" cols="20" wrap="soft" class="form-control text-center" readonly="true"><c:out value="${lot.description}"/></textarea></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="blitz.price"/> </th>
            <td><c:out value="${lot.binding.blitzPrice}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="binding.step"/> </th>
            <td><c:out value="${lot.binding.bindingStep}"/></td>
        </tr>

        <tr class="text-center">
            <th><fmt:message key="binding.participant"/> </th>
            <td><c:out value="${lot.binding.numberOfParticipant}"/></td>
        </tr>
</table>
<div class="container">
<div class=" col-md-4 col-md-offset-6">
<form class="form-signin" method="post" action="${pageContext.request.contextPath}/Control">
    <p id="message" class="has-error">${message}</p>
    <input class="btn btn-success" type="submit" value="<fmt:message key="label.add.bet"/> ">
    <input type="hidden" name="cmd" value="addBetForBlitzBindingCmd">
    <input type="hidden" name="lot_id" value="<c:out value="${lot.lotId}"/>">
    <input type="hidden" name="bet" value="<c:out value="${lot.price}"/>">
    <input type="hidden" name="bindingId" value="<c:out value="${lot.binding.id}"/>">
    <input type="hidden" name="step" value="<c:out value="${lot.binding.bindingStep}"/>">
    <input type="hidden" name="blitz" value="<c:out value="${lot.binding.blitzPrice}"/>">
    <input type="hidden" name="start" value="<c:out value="${lot.binding.start}"/>">
    <input type="hidden" name="end" value="<c:out value="${lot.binding.end}"/>">
</form>
 </div>

    <div class="col-md-4 col-md-offset-6">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelCmd">
        </form>
    </div>
    </div>
</div>
</body>
</html>
