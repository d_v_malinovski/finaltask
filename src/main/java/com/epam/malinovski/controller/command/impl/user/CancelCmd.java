package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method need to cancel adding operation
 */
public class CancelCmd implements Command {
    @Override
    public String execute(HttpServletRequest request) {
      if(request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)){
            return PagePathes.PATH_TO_ADMIN_PAGE;
        }
      if(request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {
            return PagePathes.PATH_TO_USER_PAGE;
      }
        return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
