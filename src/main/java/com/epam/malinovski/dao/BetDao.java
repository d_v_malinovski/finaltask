package com.epam.malinovski.dao;

import com.epam.malinovski.bean.Bet;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Denis
 */

public interface BetDao extends GenericDao<Bet> {
    @Override
    default void update(Bet data) throws DaoException{
        throw new UnsupportedOperationException();
    }
    @Override
    default List<Bet> select(Bet data) throws DaoException, SQLException {
        throw new UnsupportedOperationException();
    }
    @Override
    default void delete(Bet data) throws DaoException{
        throw new UnsupportedOperationException();
    }
    int getLastBetFromCurrentBinding(int bindingId) throws DaoException;
}
