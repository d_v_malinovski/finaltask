<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 10.07.2017
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session"/>
    <title><fmt:message key="label.lots"/> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">

        </div>

        <div class="collapse navbar-collapse">
            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.payment"/>">
                </div>
                <input type="hidden" name="cmd" value="redirectToPaymentCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.bindings" />">
                </div>
                <input type="hidden" name="cmd" value="redirectToBindingMenuCmd">
            </form>

            <form class="navbar-form navbar-left"  method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.lots"/>">
                </div>
                <input type="hidden" name="cmd" value="redirectToLotManagementCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.show.lots"/> ">
                </div>
                <input type="hidden" name="cmd" value="getUsersLotsCmd">
            </form>

            <form class="navbar-form navbar-left" method="get" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.add.lot"/> ">
                </div>
                <input type="hidden" name="cmd" value="redirectToAddLotCmd">
            </form>

            <form class="navbar-form navbar-right" name="changeLangForm" action="Control" method="post">
                <input type="hidden" name="cmd" value="changeLangCmd">
                <input type="hidden" name="path" value="html/showPages/lotPage.jsp">

                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="ru">
                </div>
                <div class="form-group">
                    <input class="form-control" type="submit" name="langBtn" value="en">
                </div>
            </form>

            <%-- Выйти из приложения --%>
            <form method="get" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/Control">
                <div class="form-group">
                    <input class="form-control" type="submit" value="<fmt:message key="label.log.out"/>"/>
                </div>
                <input type="hidden" name="cmd" value="logOutCmd">
            </form>
        </div>
    </div>
</nav>
<c:if test="${not empty lotList}">
<table class="table">
    <caption class=" caption text-center">
        <fmt:message key="label.lots"/>
    </caption>

    <c:forEach var="lot" items="${lotList}">
    <tr>
        <th><fmt:message key="lot.name"/> </th>
        <td><c:out value="${lot.name}"/></td>
    </tr>

    <tr>
        <th><fmt:message key="lot.state"/> </th>
        <td>
            <c:choose>
                <c:when test="${lot.state eq 1}">
                    <fmt:message key="label.sold"/>
                </c:when>

                <c:when test="${lot.state eq 2}">
                    <fmt:message key="label.blocking"/>
                </c:when>

                <c:when test="${lot.state eq 3}">
                    <fmt:message key="label.abort.sale"/>
                </c:when>

                <c:when test="${lot.state eq 4}">
                    <fmt:message key="label.send.for.sale"/>
                </c:when>

                <c:otherwise>
                    <fmt:message key="label.not.for.sale"/>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>

    <tr>
        <th><fmt:message key="lot.price"/> </th>
        <td><c:out value="${lot.price}  $"/></td>
    </tr>

    <tr>
        <th><fmt:message key="lot.description"/> </th>
        <c:set var="description" value="${lot.description}"/>
        <td><textarea maxlength ="255" cols="20" wrap="soft" class="form-control text-center" readonly="true">${description}</textarea></td>
    </tr>

        <tr>
            <td><form method="get" action="${pageContext.request.contextPath}/Control">
                <input class="btn btn-success" type="submit" value="<fmt:message key="label.update.lot"/>">
                <input type="hidden" name="lotId" value="${lot.lotId}">
                <input type="hidden" name="cmd" value="redirectToUpdateLotCmd">
            </form>

            </td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input class=" btn btn-primary" type="submit" value="<fmt:message key="label.send.for.sale"/>">
                    <input type="hidden" name="lotId" value="${lot.lotId}">
                    <input type="hidden" name="cmd" value="setLotForSaleCmd">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
</c:if>
</body>
</html>
