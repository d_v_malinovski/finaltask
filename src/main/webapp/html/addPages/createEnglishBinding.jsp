<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 21.07.2017
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setBundle basename="locale" scope="session" />
    <c:set var="localeCode" value="${pageContext.response.locale}" />
    <c:set var="ru" value="ru"/>
    <title><fmt:message key="english.binding"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.timepicker.css">

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.timepicker.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/initDate.js"></script>

    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <c:if test="${localeCode.language == ru }">
        <script src="${pageContext.request.contextPath}/js/localization/messages_ru.min.js"></script>
    </c:if>

    <script src="${pageContext.request.contextPath}/js/addBindingValidation.js"></script>
</head>
<body>
<div class="container">
 <div class="col-md-4 col-md-offset-4">
<form id="create_binding_form" class="form-signin" method="post" action="${pageContext.request.contextPath}/Control">
<input type="hidden" name="lot_id" id="lot_id" value="<c:out value="${lot.lotId}"/>" >
<p class="has-error" id="message">${message}</p>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="binding.start"/></label></h2>
    <div class="form-group">
        <input class="form-control" required type="text"  name="date_begin" placeholder="<fmt:message key="binding.start"/> " id="date_begin">
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="label.time.start.binding"/></label></h2>
    <div class="form-group">
        <input class="form-control" required type="text" name="time_begin" placeholder="<fmt:message key="label.time.start.binding"/> " id="time_begin">
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="binding.end"/></label></h2>
    <div class="form-group">
        <input class="form-control" required type="text" name="date_end" placeholder="<fmt:message key="binding.end"/> " id="date_end">
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="label.time.end.binding"/></label></h2>
    <div class="form-group">
        <input class="form-control" required type="text" name="time_end" placeholder="<fmt:message key="label.time.end.binding"/> " id="time_end">
    </div>

    <h2 class="form-signin-heading"><label class="col-sm-3 control-label"><fmt:message key="end.price"/></label></h2>
    <div class="form-group">
        <input class="form-control" type="text" required min="1" name="end_price" placeholder="<fmt:message key="end.price"/>" id="end_price">
    </div>
    <input class=" btn btn-success" type="submit" value="<fmt:message key="label.add.bindings"/> ">

    <input type="hidden" name="cmd" value="addEnglishBindingCmd">
</form>
</div>

<div class="col-md-4 col-md-offset-4">
        <form class="form-inline" method="get" action="${pageContext.request.contextPath}/Control">
            <div class="col-md-offset-1">
                <input type="submit" class="form-control btn btn-danger" value="<fmt:message key="cancel.label"/> ">
            </div>
            <input type="hidden" name="cmd" value="cancelCmd">
        </form>
    </div>
</div>
</body>
</html>
