package com.epam.malinovski.dao.impl;

import com.epam.malinovski.bean.Binding;
import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.bean.User;
import com.epam.malinovski.connectionpool.impl.ConnectionPool;
import com.epam.malinovski.constant.DBFields;
import com.epam.malinovski.constant.ExceptionMessage;
import com.epam.malinovski.constant.ExecuteQuery;
import com.epam.malinovski.dao.BindingDao;
import com.epam.malinovski.connectionpool.ConnectionLevelException;
import com.epam.malinovski.dao.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * implements interface DAO for <code>Binding</code>
 * @author Denis
 */
public class SQLBindingDao implements BindingDao {
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private SQLBindingDao(){

    }

    public static BindingDao getBindingDao(){
        return SingletonHolder.bindingDao;
    }

    private static class SingletonHolder{
        private static final BindingDao bindingDao = new SQLBindingDao();
    }

    /**
     * method create internet auction
     * @param lotId - serial number of lot
     * @param binding - information about auction with will be create
     * @throws DaoException
     */

    public void addInternetBinding(int lotId, Binding binding) throws DaoException{
        PreparedStatement preparedStatement;
        Connection connection;
        try {
            connection = pool.getConnection();

            preparedStatement = connection.prepareStatement(ExecuteQuery.GET_LAST_BINDING_ID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_INTERNET_BINDING);
                int bindingId = (rs.getInt(DBFields.ID) + 1);
                System.out.println(bindingId);

                if(bindingId == 0){
                    bindingId = 1;
                }

                binding.setId(bindingId);

                preparedStatement.setInt(1, binding.getId());
                preparedStatement.setInt(2,binding.getState());
                preparedStatement.setInt(3, binding.getType());


                Timestamp start = new Timestamp(binding.getStart().getTime());
                Timestamp end = new Timestamp(binding.getEnd().getTime());


                preparedStatement.setTimestamp(4, start);
                preparedStatement.setTimestamp(5, end);

                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_LOT_AFTER_BINDING);
                preparedStatement.setInt(1, DBFields.LOT_SOLD);
                preparedStatement.setInt(2, bindingId);
                preparedStatement.setInt(3, lotId);

                preparedStatement.executeUpdate();
            }
            rs.close();
            preparedStatement.close();
            connection.close();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

    /**
     * method create english auction
     * @param lotId - serial number of lot
     * @param binding - information about auction with will be create
     * @throws DaoException
     */

    public void addEnglishBinding(int lotId, Binding binding) throws DaoException{
        PreparedStatement preparedStatement;
        Connection connection;
        try {
            connection = pool.getConnection();

            preparedStatement = connection.prepareStatement(ExecuteQuery.GET_LAST_BINDING_ID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_ENGLISH_BINDING);
                int bindingId = rs.getInt(DBFields.ID) + 1;

                if(bindingId == 0){
                    bindingId = 1;
                }

                binding.setId(bindingId);

                preparedStatement.setInt(1, binding.getId());
                preparedStatement.setInt(2,binding.getState());
                preparedStatement.setInt(3, binding.getType());


                Timestamp start = new Timestamp(binding.getStart().getTime());
                Timestamp end = new Timestamp(binding.getEnd().getTime());


                preparedStatement.setTimestamp(4, start);
                preparedStatement.setTimestamp(5, end);
                preparedStatement.setBigDecimal(6,binding.getEndPrice());

                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_LOT_AFTER_BINDING);
                preparedStatement.setInt(1, DBFields.LOT_SOLD);
                preparedStatement.setInt(2,bindingId);
                preparedStatement.setInt(3,lotId);

                preparedStatement.executeUpdate();

            }
            rs.close();
            preparedStatement.close();
            connection.close();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }

    }

    /**
     * method create blitz auction
     * @param lotId - serial number of lot
     * @param binding - information about auction with will be create
     * @throws DaoException
     */

    public void addBlitzBinding(int lotId, Binding binding) throws DaoException{
        PreparedStatement preparedStatement;
        Connection connection;

        try {
            connection = pool.getConnection();

            preparedStatement = connection.prepareStatement(ExecuteQuery.GET_LAST_BINDING_ID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                preparedStatement = connection.prepareStatement(ExecuteQuery.ADD_BLITZ_BINDING);
                int bindingId = rs.getInt(DBFields.ID) + 1;

                if(bindingId == 0){
                    bindingId = 1;
                }

                binding.setId(bindingId);

                preparedStatement.setInt(1, binding.getId());
                preparedStatement.setInt(2,binding.getState());
                preparedStatement.setInt(3, binding.getType());

                Timestamp start = new Timestamp(binding.getStart().getTime());
                Timestamp end = new Timestamp(binding.getEnd().getTime());

                preparedStatement.setTimestamp(4, start);
                preparedStatement.setTimestamp(5, end);
                preparedStatement.setBigDecimal(6,binding.getBindingStep());
                preparedStatement.setBigDecimal(7,binding.getBlitzPrice());
                preparedStatement.setInt(8,binding.getNumberOfParticipant());

                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement(ExecuteQuery.UPDATE_LOT_AFTER_BINDING);
                preparedStatement.setInt(1, DBFields.LOT_SOLD);
                preparedStatement.setInt(2,bindingId);
                preparedStatement.setInt(3,lotId);

                preparedStatement.executeUpdate();

            }
            rs.close();
            preparedStatement.close();
            connection.close();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }

    }

    /**
     * return list bindings where user can set a bet
     * @param userId - user serial number with can be a lot owner
     * @return list of active binding
     * @throws DaoException
     */

    public List<Lot> getActiveBindings(int userId) throws DaoException{
        List<Lot> lots = new ArrayList<Lot>();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_ACTIVE_BINDING)) {

            preparedStatement.setInt(1,userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){

                Binding binding = new Binding();


                Lot lot = new Lot();

                lot.setLotId(resultSet.getInt(DBFields.LOT_ID));
                lot.setName(resultSet.getString(DBFields.LOT_NAME));
                lot.setDescription(resultSet.getString(DBFields.LOT_DESCRIPTION));
                lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));

                binding.setId(resultSet.getInt(DBFields.ID));
                binding.setState(resultSet.getInt(DBFields.BINDING_STATE));
                binding.setType(resultSet.getInt(DBFields.BINDING_TYPE));


                binding.setStart(resultSet.getTimestamp(DBFields.BINDING_START_DATE));
                binding.setEnd(resultSet.getTimestamp(DBFields.BINDING_END_DATE));

                lot.setBinding(binding);

                lots.add(lot);

            }

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return lots;
    }

    public List<Lot> getActiveBindings() throws DaoException {
        List<Lot> lots = new ArrayList<>();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_ALL_ACTIVE_BINDING)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){


                Lot lot = new Lot();

                lot.setLotId(resultSet.getInt(DBFields.LOT_ID));
                lot.setName(resultSet.getString(DBFields.LOT_NAME));
                lot.setDescription(resultSet.getString(DBFields.LOT_DESCRIPTION));
                lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));

                Binding binding = new Binding();

                binding.setId(resultSet.getInt(DBFields.ID));
                binding.setState(resultSet.getInt(DBFields.BINDING_STATE));
                binding.setType(resultSet.getInt(DBFields.BINDING_TYPE));

                binding.setStart(resultSet.getTimestamp(DBFields.BINDING_START_DATE));
                binding.setEnd(resultSet.getTimestamp(DBFields.BINDING_END_DATE));

                lot.setBinding(binding);

                lots.add(lot);

            }

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return lots;
    }

    public Binding getBindingById(int id) throws DaoException{
        Binding binding = new Binding();

        try(Connection connection = pool.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.GET_BINDING_BY_ID);){

            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                binding.setId(resultSet.getInt(DBFields.ID));
                binding.setState(resultSet.getInt(DBFields.BINDING_STATE));
                binding.setType(resultSet.getInt(DBFields.BINDING_TYPE));
                binding.setStart(resultSet.getTimestamp(DBFields.BINDING_START_DATE));
                binding.setEnd(resultSet.getTimestamp(DBFields.BINDING_END_DATE));
                binding.setNumberOfParticipant(resultSet.getInt(DBFields.BINDING_NUMBER_OF_PARTICIPANT));
                binding.setBindingStep(resultSet.getBigDecimal(DBFields.BINDING_STEP_PRICE));
                binding.setBlitzPrice(resultSet.getBigDecimal(DBFields.BINDING_BLITZ_PRICE));
                binding.setEndPrice(resultSet.getBigDecimal(DBFields.BINDING_END_PRICE));
            }
        } catch (SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch (ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return binding;
    }

    public void deleteBindingByID(int id) throws DaoException{


        try (Connection connection = pool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ExecuteQuery.DELETE_BINDING_BY_ID)){

            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

    /**
     * method find all bindings with status of binding
     * @param statusId - current state of binding (open / closed)
     * @return list of binding with current state
     * @throws DaoException
     */

    public List<Lot> getBindingsWithOpenStateAndType(int statusId) throws DaoException {
        List<Lot> lots = new ArrayList<>();

        try(Connection connection = pool.getConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement(ExecuteQuery.GET_BINDINGS_WITH_CURRENT_STATUS)) {

            preparedStatement.setInt(1,statusId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){

                Binding binding = new Binding();

                User user = new User();

                Lot lot = new Lot();

                lot.setLotId(resultSet.getInt(DBFields.LOT_ID));
                lot.setName(resultSet.getString(DBFields.LOT_NAME));
                lot.setDescription(resultSet.getString(DBFields.LOT_DESCRIPTION));
                lot.setPrice(resultSet.getBigDecimal(DBFields.LOT_PRICE));

                binding.setId(resultSet.getInt(DBFields.ID));
                binding.setState(resultSet.getInt(DBFields.BINDING_STATE));
                binding.setType(resultSet.getInt(DBFields.BINDING_TYPE));
                binding.setStart(resultSet.getTimestamp(DBFields.BINDING_START_DATE));
                binding.setEnd(resultSet.getTimestamp(DBFields.BINDING_END_DATE));

                user.setId(resultSet.getInt(DBFields.OWNER));

                lot.setBinding(binding);
                lot.setUser(user);

                lots.add(lot);

            }

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
        return lots;
    }

    public void closeBinding(int bindingId, int userId, int lotId) throws DaoException{
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();

            preparedStatement = connection.prepareStatement(ExecuteQuery.CLOSE_BINDING);
            preparedStatement.setInt(1,DBFields.BINDING_CLOSE);
            preparedStatement.setInt(2,bindingId);

            preparedStatement.executeUpdate();

            int lot_owner_id = 0;

            preparedStatement = connection.prepareStatement(ExecuteQuery.GET_PRODUCT_OWNER_ID_BY_LOT_ID);
            preparedStatement.setInt(1,lotId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                lot_owner_id = resultSet.getInt(DBFields.OWNER);

                preparedStatement = connection.prepareStatement(ExecuteQuery.GET_BILLING_BY_USER_ID);
                preparedStatement.setInt(1,lot_owner_id);

                resultSet = preparedStatement.executeQuery();

                if(resultSet.next()){
                    int billing_id = resultSet.getInt(DBFields.ID);

                    preparedStatement = connection.prepareStatement(ExecuteQuery.PUT_PRODUCT_FOR_PAYMENT);
                    preparedStatement.setInt(1, userId);
                    preparedStatement.setInt(2, lotId);
                    preparedStatement.setInt(3, billing_id);

                    preparedStatement.executeUpdate();
                }

            }

            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch(SQLException e) {
            throw new DaoException(ExceptionMessage.SQL_FAIL);
        } catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionMessage.CONNECTION_FAIL);
        }
    }

}
