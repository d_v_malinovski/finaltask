package com.epam.malinovski.controller.command.impl.binding;

import com.epam.malinovski.bean.Lot;
import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.Attributes;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;
import com.epam.malinovski.service.LotService;
import com.epam.malinovski.service.ServiceException;
import com.epam.malinovski.service.impl.LotServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * Method get page for auction addition
 */
public class RedirectToCreateBindingCmd implements Command {
    private static final LotService lotService = LotServiceImpl.getLotService();

    @Override
    public String execute(HttpServletRequest request) {
       if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)){
           int id = Integer.parseInt(request.getParameter(Attributes.LOT_ID));
           Lot lot = null;
           String bindingType = request.getParameter(Attributes.BINDING_TYPE);

           try {
               lot = lotService.getLotById(id);
           } catch (ServiceException e) {
               e.printStackTrace();
           }
           request.getSession().setAttribute(Attributes.LOT,lot);

           if(bindingType.equals(Attributes.SHORT_NAME_BLITZ_AUCTION))
               return PagePathes.PATH_TO_ADD_BLITZ_BINDING;

           if(bindingType.equals(Attributes.SHORT_NAME_ENGLISH_AUCTION))
               return PagePathes.PATH_TO_ADD_ENGLISH_BINDING;

           if(bindingType.equals(Attributes.SHORT_NAME_INTERNET_AUCTION))
               return PagePathes.PATH_TO_ADD_INTERNET_BINDING;

       }
        return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
