package com.epam.malinovski.controller.command.impl.user;

import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Denis
 * Registration page for new user
 */
public class RedirectToRegistrationCmd implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        session.setAttribute(AccessConstants.ROLE, AccessConstants.GUEST_ROLE);
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.GUEST_ROLE)){
            return PagePathes.PATH_TO_REGISTRATION_PAGE;
        }else
            return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
