package com.epam.malinovski.controller.command.impl.user;


import com.epam.malinovski.constant.AccessConstants;
import com.epam.malinovski.constant.PagePathes;
import com.epam.malinovski.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Denis
 * This method redirect to add user page
 */
public class RedirectToAddUserCmd implements Command {
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.ADMIN_ROLE)) {
            return PagePathes.PATH_TO_ADD_USER_PAGE;
        }else {
            return PagePathes.PATH_TO_ERROR_PAGE;
        }
    }
}
