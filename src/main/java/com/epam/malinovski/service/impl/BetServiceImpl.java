package com.epam.malinovski.service.impl;

import com.epam.malinovski.bean.Bet;
import com.epam.malinovski.dao.BetDao;
import com.epam.malinovski.dao.DaoException;
import com.epam.malinovski.dao.DaoFactory;
import com.epam.malinovski.service.BetService;
import com.epam.malinovski.service.ServiceException;

/**
 * @author Denis
 */
public class BetServiceImpl implements BetService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();

    private static final class SingletonHolder{
        private static final BetService betService = new BetServiceImpl();
    }

    public static BetService getBetService(){
        return SingletonHolder.betService;
    }

    @Override
    public int getLastBetFromCurrentBinding(int bindingId) throws ServiceException {
        try {
            BetDao betDao = daoFactory.getBetDao();
            return betDao.getLastBetFromCurrentBinding(bindingId);
        }catch (DaoException ex){
            throw new ServiceException("Bet Service", ex);
        }
    }

    @Override
    public void save(Bet data) throws ServiceException {
        try {
            BetDao betDao = daoFactory.getBetDao();
            betDao.insert(data);
        }catch (DaoException ex){
            throw new ServiceException("Bet Service", ex);
        }
    }
}
